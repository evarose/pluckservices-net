﻿using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.PointsAndBadging;
using Pluck.SiteLife.SDK.Models.System;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.PointsAndBadging;
using Pluck.SiteLife.SDK.Responses;
using Pluck.SiteLife.SDK.Responses.PointsAndBadging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace PluckIL.Helpers
{
    public class RewardHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        public static bool AwardPointsToUser(string userKey, string currencyType, int totalPoints, out string pluckStatus)
        {
            pluckStatus = String.Empty;

            if (!String.IsNullOrEmpty(userKey))
            {
                AwardPointsActionRequest request1 = new AwardPointsActionRequest();
                request1.UserKey = new Pluck.SiteLife.SDK.Models.Users.UserKey { Key = userKey };
                request1.Points = totalPoints;
                request1.CurrencyType = currencyType;

                UserPointsSummaryRequest request2 = new UserPointsSummaryRequest();
                request2.CurrencyType = currencyType;
                request2.UserKey = new Pluck.SiteLife.SDK.Models.Users.UserKey { Key = userKey };

                RequestBatch requests = new RequestBatch();
                requests.AddRequest(request1);
                requests.AddRequest(request2);

                ResponseBatch response = SDKRequests.GetBatchResponse(requests, pluckHost, false);
                if (response != null)
                {
                    foreach (ResponseEnvelope envelope in response.Envelopes)
                    {
                        IResponse envResponse = envelope.GetResponse();
                        if (envResponse.ResponseStatus.StatusCode.Equals(ResponseStatusCode.OK))
                        {
                            pluckStatus = CustomMessage.PointsAwarded;
                            if (envResponse is UserPointsSummaryResponse)
                            {
                                pluckStatus += " User points for currency '" + currencyType + "' is now " + ((UserPointsSummaryResponse)envResponse).Points.ToString();
                            }
                        }
                        else
                        {
                            pluckStatus = CustomMessage.PluckResponseError + envResponse.ResponseStatus.StatusCode.ToString();
                            return false;
                        }
                    }
                    return true;
                }
                else
                {
                    pluckStatus = CustomMessage.UserNotFound;
                }
            }
            return false;
        }

        public static int GetPointsForUser(string userKey, string currencyType, out string pluckStatus)
        {
            int totalPoints = 0;
            pluckStatus = String.Empty;

            if (!String.IsNullOrEmpty(userKey))
            {
                UserPointsSummaryRequest request = new UserPointsSummaryRequest();
                request.CurrencyType = currencyType;
                request.UserKey = new Pluck.SiteLife.SDK.Models.Users.UserKey { Key = userKey };

                RequestBatch requests = new RequestBatch();
                requests.AddRequest(request);

                IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, false);
                if (response != null)
                {
                    ResponseStatus statusResponse = response.ResponseStatus;
                    if (statusResponse.StatusCode.Equals(ResponseStatusCode.OK))
                    {
                        return ((UserPointsSummaryResponse)response).Points;
                    }
                    else
                    {
                        pluckStatus = CustomMessage.PluckResponseError + statusResponse.StatusCode.ToString();
                    }
                }
                else
                {
                    pluckStatus = CustomMessage.UserNotFound; ;
                }
            }

            return totalPoints;
        }

        public static Array GetPointsAndBadgingActivity(string userKey, out string pluckStatus)
        {
            pluckStatus = String.Empty;
            PointsAndBadgingActivityPageRequest request = new PointsAndBadgingActivityPageRequest();
            request.UserKey = new Pluck.SiteLife.SDK.Models.Users.UserKey { Key = userKey };
            request.ItemsPerPage = 20;
            request.OneBasedOnPage = 1;

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);

            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, true);
            if (response != null)
            {
                ResponseStatus statusResponse = response.ResponseStatus;
                if (statusResponse.StatusCode.Equals(ResponseStatusCode.OK))
                {
                    var activities = from a in ((PointsAndBadgingActivityPageResponse)response).Items
                                   select new ViewModels.PointsAndBadgingActivityModel
                                   {
                                       ActivityDate = a.ActivityDate,
                                       ActivityName = a.ActivityName,
                                       CurrencyType = a.CurrencyType,
                                       ActivityTags = String.Join(",", a.ActivityTags),
                                       Points = a.Points
                                   };
                    if (activities.Count() > 0)
                    {
                        pluckStatus = "Found " + activities.Count().ToString() + " total activities";
                    }
                    else
                    {
                        pluckStatus = CustomMessage.ResultsNotFound;
                    }
                    return activities.ToArray();
                }
                else
                {
                    pluckStatus = CustomMessage.PluckResponseError + statusResponse.StatusCode.ToString();
                }
            }
            else
            {
                pluckStatus = CustomMessage.PluckNoResponse;
            }

            return null;
        }

        public static bool SendBadgingEvent(string activityName, string[] activityTags, string[] userKeys, out string pluckStatus)
        {
            pluckStatus = String.Empty;
            BadgingEventActionRequest request = new BadgingEventActionRequest();
            request.ActivityName = activityName;
            List<string> tags = new List<string>();
            foreach (string activityTag in activityTags)
            {
                tags.Add("Section:" + activityTag);
            }
            request.ActivityTags = tags.ToArray();
            request.UserTags = userKeys;

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);

            // Sending this request as a current logged in user
            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, false);
            if (response != null)
            {
                ResponseStatus statusResponse = response.ResponseStatus;
                if (statusResponse.StatusCode.Equals(ResponseStatusCode.OK))
                {
                    pluckStatus = CustomMessage.BadgingEventCompleted;
                    return true;
                }
                else
                {
                    pluckStatus = CustomMessage.PluckResponseError + statusResponse.StatusCode.ToString();
                }
            }
            else
            {
                pluckStatus = CustomMessage.PluckNoResponse;
            }

            return false;
        }
    }
}
