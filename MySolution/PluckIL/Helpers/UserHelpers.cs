﻿using Pluck.SiteLife.SDK.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluckIL.Helpers
{
    public class UserHelpers
    {
        public static Array GetUserTiers()
        {
            UserTier[] actionTypes = (UserTier[])Enum.GetValues(typeof(UserTier));
            return actionTypes.OrderBy(a => a.ToString()).ToArray();
        }
    }
}
