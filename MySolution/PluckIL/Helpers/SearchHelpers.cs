﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.Common;
using Pluck.SiteLife.SDK.Models.System;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.Common;
using Pluck.SiteLife.SDK.Responses.Common;

namespace PluckIL
{
    public class SearchHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        /// <summary>
        /// Gets Pluck search types.
        /// </summary>
        /// <returns></returns>
        public static Array GetSearchTypes()
        {
            SearchTypeEnum[] searchTypes = (SearchTypeEnum[])Enum.GetValues(typeof(SearchTypeEnum));
            return searchTypes.Where(s => s.ToString() != "All").OrderBy(s => s.ToString()).ToArray();
        }

        /// <summary>
        /// Searches Pluck content.
        /// </summary>
        /// <param name="searchType">Type of the search.</param>
        /// <param name="keyword">The keyword.</param>
        /// <param name="totalItems">The total items.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="pluckStatus">The Pluck status.</param>
        /// <returns></returns>
        public static Array SearchPluck(string searchType, string keyword, out int totalItems, out string objectType, out string pluckStatus)
        {
            totalItems = 0;
            objectType = String.Empty;
            pluckStatus = String.Empty;

            dynamic obj;
            string message;
            if (!PluckConfig.CheckPluckStatus(out obj, out message))
            {
                pluckStatus = message;
                return null;
            }

            if (!String.IsNullOrEmpty(keyword))
            {
                SearchTypeEnum[] searchTypes = (SearchTypeEnum[])Enum.GetValues(typeof(SearchTypeEnum));
                SearchActionRequest request = new SearchActionRequest();
                request.SearchType = searchTypes.Where(s => s.ToString() == searchType).FirstOrDefault();
                request.SearchString = keyword;
                request.ItemsPerPage = 10;
                request.OneBasedOnPage = 1;

                RequestBatch requests = new RequestBatch();
                requests.AddRequest(request);

                IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, true);
                if (response != null)
                {
                    ResponseStatus statusResponse = response.ResponseStatus;
                    if (statusResponse.StatusCode.Equals(ResponseStatusCode.OK))
                    {
                        SearchResponse searchResponse = (SearchResponse)response;
                        if (searchResponse != null)
                        {
                            List<SearchResults> results = new List<SearchResults>() { searchResponse.SearchResults };
                            if (results.Count() > 0)
                            {
                                totalItems = (int)results[0].TotalItems;
                                objectType = totalItems > 0 ? results[0].Items[0].ObjectType.ToString() : CustomMessage.ResultsNotFound;
                                return results[0].Items;
                            }
                        }
                        else
                        {
                            pluckStatus = CustomMessage.ResultsNotFound;
                        }
                    }
                    else
                    {
                        pluckStatus = CustomMessage.PluckResponseError + statusResponse.StatusCode.ToString();
                    }
                }
            }
            return null;
        }
    }
}
