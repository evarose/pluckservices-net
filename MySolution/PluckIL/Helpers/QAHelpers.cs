﻿using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.Discovery;
using Pluck.SiteLife.SDK.Models.External;
using Pluck.SiteLife.SDK.Models.Reactions;
using Pluck.SiteLife.SDK.Models.System.Filtering;
using Pluck.SiteLife.SDK.Models.System.Sorting;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.Discovery;
using Pluck.SiteLife.SDK.Requests.External;
using Pluck.SiteLife.SDK.Requests.Reactions;
using Pluck.SiteLife.SDK.Requests.Users;
using Pluck.SiteLife.SDK.Responses;
using Pluck.SiteLife.SDK.Responses.Discovery;
using Pluck.SiteLife.SDK.Responses.External;
using Pluck.SiteLife.SDK.Responses.Reactions;
using Pluck.SiteLife.SDK.Responses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace PluckIL.Helpers
{
    public class QAHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        public static Dictionary<string, string> ListCategories()
        {
            Dictionary<string, string> categories = new Dictionary<string, string>();
            categories.Add("general", "General");
            categories.Add("coffee-machines", "Coffee Machines");
            categories.Add("entertainment", "Entertainment");
            categories.Add("music", "Music");
            categories.Add("travel", "Travel");

            return categories;
        }

        public static bool CreateQuestionAnswerItem(ViewModels.ArticleModel article, string question)
        {
            List<DiscoveryCategory> categories = new List<DiscoveryCategory>();

            // Add the comment/question first
            string questionKey = article.ArticleKey + "-Q";
            CommentActionRequest request1 = new CommentActionRequest();
            request1.Body = question;
            request1.CommentedOnKey = new ExternalResourceKey { Key = questionKey };
            request1.OnPageTitle = "Q: " + article.ArticleTitle;
            request1.OnPageUrl = article.ArticleUrl + questionKey;
            // Assign "question" to this article category in addition to user selected category
            categories.Add(new DiscoveryCategory { Name = "question" }); 
            categories.Add(new DiscoveryCategory { Name = article.Categories });
            request1.Categories = categories.ToArray();
            request1.Section = new DiscoverySection { Name = article.Section };

            // Create article object representing Expert Answer
            string answerKey = article.ArticleKey + "-E";
            UpdateArticleActionRequest request2 = new UpdateArticleActionRequest();
            request2.ArticleKey = new ExternalResourceKey { Key = answerKey };
            categories.Clear();
            // Assign "expert-answer" to this article category in addition to user selected category
            categories.Add(new DiscoveryCategory { Name = "expert-answer" });
            categories.Add(new DiscoveryCategory { Name = article.Categories });
            request2.Categories = categories.ToArray();
            request2.OnPageTitle = "E: " + article.ArticleTitle;
            request2.OnPageUrl = article.ArticleUrl + questionKey;
            request2.Section = new DiscoverySection { Name = article.Section };

            // Create article object representing Replies/Answers
            string repliesKey = article.ArticleKey + "-R";
            UpdateArticleActionRequest request3 = new UpdateArticleActionRequest();
            request3.ArticleKey = new ExternalResourceKey { Key = repliesKey };
            categories.Clear();
            // Assign "replies" to this article category in addition to user selected category
            categories.Add(new DiscoveryCategory { Name = "replies" });
            categories.Add(new DiscoveryCategory { Name = article.Categories });
            request3.Categories = categories.ToArray();
            request3.OnPageTitle = "R: " + article.ArticleTitle;
            request3.OnPageUrl = article.ArticleUrl + questionKey;
            request3.Section = new DiscoverySection { Name = article.Section };

            RequestBatch requests1 = new RequestBatch();
            requests1.AddRequest(request1);

            RequestBatch requests2 = new RequestBatch();
            requests2.AddRequest(request2);
            requests2.AddRequest(request3);

            if (CreateQuestion(requests1) && CreateAnswers(requests2))
                return true;

            return false;
        }

        private static bool CreateQuestion(RequestBatch requests)
        {
            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, false);
            if (response != null)
            {
                if (response.ResponseStatus.StatusCode == Pluck.SiteLife.SDK.Models.System.ResponseStatusCode.OK)
                    return true;
            }
            return false;
        }

        private static bool CreateAnswers(RequestBatch requests)
        {
            ResponseBatch response = SDKRequests.GetBatchResponse(requests, pluckHost, true);
            if (response != null)
            {
                foreach (ResponseEnvelope envelope in response.Envelopes)
                {
                    IResponse envResponse = envelope.GetResponse();
                    if (envResponse.ResponseStatus.StatusCode != Pluck.SiteLife.SDK.Models.System.ResponseStatusCode.OK)
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public static void CheckExpertAnswer(string articleKey, string currentUserKey, out int totalComments, out string currentUserTier)
        {
            totalComments = 0;
            currentUserTier = String.Empty;

            ArticleRequest request1 = new ArticleRequest();
            request1.ArticleKey = new Pluck.SiteLife.SDK.Models.External.ExternalResourceKey { Key = articleKey };

            UserRequest request2 = new UserRequest();
            request2.UserKey = new Pluck.SiteLife.SDK.Models.Users.UserKey { Key = currentUserKey };

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request1);
            requests.AddRequest(request2);

            ResponseBatch response = SDKRequests.GetBatchResponse(requests, pluckHost, false);
            if (response != null)
            {
                foreach (ResponseEnvelope envelope in response.Envelopes)
                {
                    IResponse envResponse = envelope.GetResponse();
                    if (envResponse.ResponseStatus.StatusCode == Pluck.SiteLife.SDK.Models.System.ResponseStatusCode.OK)
                    {
                        if (envResponse is UserResponse)
                        {
                            currentUserTier = ((UserResponse)envResponse).User.UserTier.ToString();
                        }

                        if (envResponse is ArticleResponse)
                        {
                            Article article = ((ArticleResponse)envResponse).Article;
                            if (article.ArticleKey.Key == articleKey)
                                totalComments = (int)article.Comments.NumberOfTopLevelComments;
                        }
                    }
                }
            }
        }

        public static Array GetRecentQA(string section, List<string> categories, int totalQuestionsToFetch)
        {
            if (!String.IsNullOrEmpty(section) && categories.Count() > 0)
            {              
                // First, try to get content from Discovery first, if none return, then do the search instead
                var questions = GetRecentQuestions(section, categories, totalQuestionsToFetch);
                if (questions.Count == 0 || questions == null)
                {
                    int totalQuestions = 0;
                    string objectType = String.Empty;
                    string pluckStatus = String.Empty;
                    Array searchResults = SearchHelpers.SearchPluck("Comment", "Section:" + section + " AND Category:" + string.Join(",",categories), out totalQuestions, out objectType, out pluckStatus);
                    var articles = from item in searchResults.Cast<IDaapiModel>()
                                   select new ViewModels.QADiscoveryModel
                                   {
                                       QuestionKey = ((ExternalResourceKey)((Comment)item).CommentedOnKey).Key,
                                       QuestionTitle = GetArticleTitle(((ExternalResourceKey)((Comment)item).CommentedOnKey).Key),
                                       QuestionCategories = GetDiscoveryCategories(((Comment)item).Categories)
                                   };
                    questions = articles.ToList();
                }

                // When you get here, you get all the article keys for recent questions. Now get the comment object for each question, answer & replies and batch them into one request (in order to improve Pluck performance), rather than making separate request for each
                RequestBatch requests = new RequestBatch();
                foreach (ViewModels.QADiscoveryModel question in questions)
                {
                    // This is for question
                    CommentsPageRequest request = new CommentsPageRequest();
                    request.CommentedOnKey = new ExternalResourceKey { Key = question.QuestionKey };
                    request.SortType = new TimestampSort { SortOrder = SortOrder.Descending };
                    request.FilterType = new ThreadFilter { ThreadPath = "/" }; // Only get top level comments
                    request.ItemsPerPage = 1;
                    request.OneBasedOnPage = 1;
                    requests.AddRequest(request);
                }

                foreach (ViewModels.QADiscoveryModel question in questions)
                {
                    // This is for answer
                    CommentsPageRequest request = new CommentsPageRequest();
                    request.CommentedOnKey = new ExternalResourceKey { Key = question.QuestionKey.Replace("-Q", "-E") };
                    request.SortType = new TimestampSort { SortOrder = SortOrder.Descending };
                    request.FilterType = new ThreadFilter { ThreadPath = "/" }; // Only get top level comments
                    request.ItemsPerPage = 1;
                    request.OneBasedOnPage = 1;
                    requests.AddRequest(request);
                }

                foreach (ViewModels.QADiscoveryModel question in questions)
                {
                    // This is for replies
                    CommentsPageRequest request = new CommentsPageRequest();
                    request.CommentedOnKey = new ExternalResourceKey { Key = question.QuestionKey.Replace("-Q", "-R") };
                    request.SortType = new TimestampSort { SortOrder = SortOrder.Descending };
                    request.FilterType = new ThreadFilter { ThreadPath = "/" }; // Only get top level comments
                    request.ItemsPerPage = 50;
                    request.OneBasedOnPage = 1;
                    requests.AddRequest(request);
                }

                List<ViewModels.QADiscoveryModel> qaItems = new List<ViewModels.QADiscoveryModel>();

                ResponseBatch response = SDKRequests.GetBatchResponse(requests, pluckHost, false);
                if (response != null)
                {
                    foreach (ResponseEnvelope envelope in response.Envelopes)
                    {
                        IResponse envResponse = envelope.GetResponse();
                        if (envResponse.ResponseStatus.StatusCode == Pluck.SiteLife.SDK.Models.System.ResponseStatusCode.OK)
                        {
                            if (envResponse is CommentsPageResponse)
                            {
                                List<Comment> comments = ((CommentsPageResponse)envResponse).Items.ToList();
                                // Building the question item in here
                                var question = comments.Where(c => ((ExternalResourceKey)c.CommentedOnKey).Key.Contains("-Q")).FirstOrDefault();
                                if (question != null)
                                {
                                    var qItem = from q in questions
                                                join c in comments on q.QuestionKey equals ((ExternalResourceKey)question.CommentedOnKey).Key
                                                select new ViewModels.QADiscoveryModel
                                                {
                                                    QuestionKey = q.QuestionKey,
                                                    QuestionTitle = q.QuestionTitle,
                                                    QuestionBody = c.Body,
                                                    QuestionDatePosted = c.PostedAtTime,
                                                    QuestionerName = c.Owner.DisplayName,
                                                    QuestionerKey = c.Owner.UserKey.Key,
                                                    QuestionerAvatarUrl = c.Owner.AvatarPhotoUrl,
                                                    QuestionCategories = q.QuestionCategories
                                                };
                                    qaItems.AddRange(qItem);
                                }

                                // Building the answer item in here
                                var answer = comments.Where(c => ((ExternalResourceKey)c.CommentedOnKey).Key.Contains("-E")).FirstOrDefault();
                                if (answer != null)
                                {
                                    foreach (ViewModels.QADiscoveryModel qItem in qaItems)
                                    {
                                        if (qItem.QuestionKey == ((ExternalResourceKey)answer.CommentedOnKey).Key.Replace("-E","-Q"))
                                        {
                                            qItem.AnswerKey = ((ExternalResourceKey)answer.CommentedOnKey).Key;
                                            qItem.AnswerBody = answer.Body;
                                            qItem.AnswerDatePosted = answer.PostedAtTime;
                                            qItem.AnswererName = answer.Owner.DisplayName;
                                            qItem.AnswererAvatarUrl = answer.Owner.AvatarPhotoUrl;
                                        }
                                    }
                                }

                                // Get number of replies here
                                var replies = comments.Where(c => ((ExternalResourceKey)c.CommentedOnKey).Key.Contains("-R")).FirstOrDefault();
                                if (replies != null)
                                {
                                    foreach (ViewModels.QADiscoveryModel qItem in qaItems)
                                    {
                                        if (qItem.QuestionKey == ((ExternalResourceKey)replies.CommentedOnKey).Key.Replace("-R", "-Q"))
                                        {
                                            // You can extract the below property, but REMEMBER this property WILL NOT be returned after FTI. So use an alternative method to get real-time number of replies.
                                            //qItem.NumberOfReplies = (int)((Article)replies.Parent).Comments.NumberOfTopLevelComments;
                                            qItem.NumberOfReplies = comments.Count();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return qaItems.OrderByDescending(q => q.QuestionDatePosted).ToArray();
                
            }
            return null;
        }

        private static List<ViewModels.QADiscoveryModel> GetRecentQuestions(string section, List<string> categories, int totalQuestionsToFetch)
        {
            DiscoverContentActionRequest request = new DiscoverContentActionRequest();
            request.Activity = DiscoveryActivity.Recent;
            request.Type = ContentType.Article;
            request.Sections = new DiscoverySection[] { new DiscoverySection() { Name = section } };
            List<DiscoveryCategory> discoveryCategories = new List<DiscoveryCategory>();
            foreach (string category in categories)
            {
                discoveryCategories.Add(new DiscoveryCategory { Name = category });
            }
            request.Categories = discoveryCategories.ToArray();
            request.Age = 30;
            request.MaximumNumberOfDiscoveries = totalQuestionsToFetch;

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);

            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, false);
            if (response != null)
            {
                if (response.ResponseStatus.StatusCode == Pluck.SiteLife.SDK.Models.System.ResponseStatusCode.OK)
                {
                    DiscoverContentResponse discoResponse = (DiscoverContentResponse)response;
                    var articles = from item in discoResponse.DiscoveredContent
                                   select new ViewModels.QADiscoveryModel {
                                       QuestionKey = ((Article)item).ArticleKey.Key,
                                       QuestionTitle = ((Article)item).Title,
                                       QuestionCategories = GetDiscoveryCategories(((Article)item).Categories)
                                   };
                    return articles.ToList();
                }
            }

            return null;
        }

        private static string GetDiscoveryCategories(DiscoveryCategory[] categories)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < categories.Count(); i++)
            {
                if (categories[i].Name != "question")
                {
                    if (i == categories.Count() - 1)
                        sb.Append(categories[i].Name);
                    else
                        sb.Append(categories[i].Name + ",");
                }
            }
            return sb.ToString();
        }

        private static string GetArticleTitle(string articleKey)
        {
            ArticleRequest request = new ArticleRequest { ArticleKey = new ExternalResourceKey { Key = articleKey }, ViewTrackRequest = false };
            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);
            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, false);
            if (response != null)
            {
                if (response.ResponseStatus.StatusCode == Pluck.SiteLife.SDK.Models.System.ResponseStatusCode.OK)
                {
                    return ((ArticleResponse)response).Article.Title;
                }
            }
            return String.Empty;
        }
    }
}
