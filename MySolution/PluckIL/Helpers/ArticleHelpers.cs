﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.Discovery;
using Pluck.SiteLife.SDK.Models.External;
using Pluck.SiteLife.SDK.Models.System;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.External;
using Pluck.SiteLife.SDK.Responses.External;

namespace PluckIL
{
    public class ArticleHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        /// <summary>
        /// Gets the article.
        /// </summary>
        /// <param name="articleKey">The article key.</param>
        /// <param name="pluckStatus">The Pluck status.</param>
        /// <returns></returns>
        public static Array GetArticle(string articleKey, out string pluckStatus)
        {
            pluckStatus = String.Empty;

            dynamic obj;
            string message;
            if (!PluckConfig.CheckPluckStatus(out obj, out message))
            {
                pluckStatus = message;
                return null;
            }

            ArticleRequest request = new ArticleRequest();
            request.ArticleKey = new ExternalResourceKey { Key = articleKey };

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);


            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, false);
            if (response != null)
            {
                ResponseStatus statusResponse = response.ResponseStatus;
                if (statusResponse.StatusCode.Equals(ResponseStatusCode.OK))
                {
                    ArticleResponse articleResponse = (ArticleResponse)response;
                    if (articleResponse != null)
                    {
                        List<Article> articleData = new List<Article>() { articleResponse.Article };

                        List<string> categories = new List<string>();
                        foreach (DiscoveryCategory category in articleData.FirstOrDefault().Categories)
                        {
                            categories.Add(category.Name);
                        }

                        var article = from a in articleData
                                      select new ViewModels.ArticleModel
                                      {
                                          ArticleKey = a.ArticleKey.Key,
                                          ArticleTitle = a.Title,
                                          ArticleUrl = a.Url,
                                          Categories = a.Categories != null ? string.Join(", ", categories) : String.Empty,
                                          Section = a.Section != null ? a.Section.Name : String.Empty,
                                          NumberOfReviews = a.Reviews.NumberOfReviews,
                                          NumberOfComments = a.Comments.NumberOfComments,
                                          NumberOfTopLevelComments = a.Comments.NumberOfTopLevelComments,
                                          IsReview = a.Comments.NumberOfComments == 0 && a.Reviews.NumberOfReviews >= 0 ? true : false,
                                          ReviewItemState = a.ReviewItemState.ToString(),
                                          NumberOfReviewComments = (uint)ReviewHelpers.GetTotalReviewComments(a.ArticleKey.Key),
                                          SiteOfOrigin = a.SiteOfOrigin.Key,
                                          SourceSiteName = (a.SourceSite != null) ? a.SourceSite.Name : String.Empty,
                                          SourceSiteKey = (a.SourceSite != null) ? a.SourceSite.SourceSiteKey.Key : String.Empty,
                                      };
                        return article.ToArray();
                    }
                }
                else
                {
                    pluckStatus = CustomMessage.PluckResponseError + statusResponse.StatusCode.ToString();
                }
            }
            else
            {
                pluckStatus = CustomMessage.ArticleNotFound;
            }
            return null;
        }

        /// <summary>
        /// Updates the article.
        /// </summary>
        /// <param name="articleData">The article data.</param>
        /// <param name="responseStatus">The response status.</param>
        public static void UpdateArticle(Dictionary<string, string> articleData, out string responseStatus)
        {
            responseStatus = String.Empty;
            dynamic obj;
            string message;
            if (!PluckConfig.CheckPluckStatus(out obj, out message))
            {
                responseStatus = message;
                return;
            }

            UpdateArticleActionRequest request = new UpdateArticleActionRequest();
            request.ArticleKey = new ExternalResourceKey { Key = articleData["articleKey"] };
            request.OnPageTitle = articleData["articleTitle"];
            request.OnPageUrl = articleData["articleUrl"];
            var catList = from categoryName in articleData["categories"].Split(',')
                          select new DiscoveryCategory
                          {
                              Name = categoryName
                          };
            request.Categories = catList.ToArray();
            request.Section = new DiscoverySection { Name = articleData["section"] };

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);
            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, true);
            if (response != null)
            {
                ResponseStatus statusResponse = response.ResponseStatus;
                if (statusResponse.StatusCode.Equals(ResponseStatusCode.OK))
                {
                    responseStatus = CustomMessage.ArticleUpdated;
                }
                else
                {
                    responseStatus = CustomMessage.PluckResponseError + statusResponse.StatusCode.ToString();
                }
            }
        }
    }
}
