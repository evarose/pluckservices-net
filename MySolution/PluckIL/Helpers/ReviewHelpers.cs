﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Pluck.SiteLife.SDK.Models.External;
using Pluck.SiteLife.SDK.Models.Reactions.Reviews;
using Pluck.SiteLife.SDK.Models.Users;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.Reactions.Reviews;
using Pluck.SiteLife.SDK.Requests.Users;
using Pluck.SiteLife.SDK.Responses.Reactions.Reviews;
using Pluck.SiteLife.SDK.Responses.Users;

namespace PluckIL
{
    public class ReviewHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        /// <summary>
        /// Gets the total user reviews.
        /// </summary>
        /// <param name="userKey">The user key.</param>
        /// <returns></returns>
        public static int GetTotalUserReviews(string userKey)
        {
            UserReviewsPageRequest request = new UserReviewsPageRequest();
            request.UserKey = new UserKey { Key = userKey };
            request.OneBasedOnPage = 1;
            request.ItemsPerPage = 50;

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);

            return (int)((UserReviewsPageResponse)SDKRequests.GetSingleResponse(requests, pluckHost, false)).TotalItems;
        }

        /// <summary>
        /// Gets the total review comments.
        /// </summary>
        /// <param name="articleKey">The article key.</param>
        /// <returns></returns>
        public static int GetTotalReviewComments(string articleKey)
        {
            ReviewsPageRequest request = new ReviewsPageRequest();
            request.ReviewedKey = new ExternalResourceKey { Key = articleKey };
            request.ItemsPerPage = 20;
            request.OneBasedOnPage = 1;

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);

            List<Review> reviewList = new List<Review>(((ReviewsPageResponse)SDKRequests.GetSingleResponse(requests, pluckHost, false)).Items);

            int commentsCount = 0;

            foreach (Review review in reviewList)
            {
                commentsCount = commentsCount + (int)review.Comments.NumberOfComments;
            }

            return commentsCount;
        }
    }
}
