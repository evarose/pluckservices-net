﻿using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.Discovery;
using Pluck.SiteLife.SDK.Models.External;
using Pluck.SiteLife.SDK.Models.Reactions;
using Pluck.SiteLife.SDK.Models.System.Filtering;
using Pluck.SiteLife.SDK.Models.System.Sorting;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.Reactions;
using Pluck.SiteLife.SDK.Responses;
using Pluck.SiteLife.SDK.Responses.Reactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace PluckIL.Helpers
{
    public class CommentHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        public static List<Comment> GetCommentsFromRecentArticles(string articleSection, string[] articleCategories, int maxNumberOfCommentsPerArticle, out int totalComments)
        {
            totalComments = 0;
            ViewModels.DiscoverContentModel discoRequest = new ViewModels.DiscoverContentModel();
            discoRequest.DiscoveryActivity = DiscoveryActivity.Recent;
            discoRequest.ContentType = ContentType.Article;
            discoRequest.DiscoverySections = new DiscoverySection[] { new DiscoverySection() { Name = articleSection } };
            List<DiscoveryCategory> discoveryCategories = new List<DiscoveryCategory>();
            foreach (string category in articleCategories)
            {
                discoveryCategories.Add(new DiscoveryCategory { Name = category });
            }
            discoRequest.DiscoveryCategories = discoveryCategories.ToArray();
            discoRequest.Age = 15;
            discoRequest.MaximumNumberOfDiscoveries = 5; // Number of articles to return

            // Get recent articles from discovery
            int totalArticles = 0;
            List<Article> articles = new List<Article>();
            foreach (IDaapiModel item in DiscoveryHelpers.DiscoverContent(discoRequest, out totalArticles))
            {
                articles.Add((Article)item);
            }

            // For each recent article, get recent comments here
            TimestampSort dateSort = new TimestampSort();
            dateSort.SortOrder = SortOrder.Descending;

            // Only get top level comments
            ThreadFilter threadFilter = new ThreadFilter();
            threadFilter.ThreadPath = "/";

            RequestBatch requests = new RequestBatch();
            foreach (Article article in articles)
            {
                CommentsPageRequest request = new CommentsPageRequest();
                request.CommentedOnKey = new ExternalResourceKey { Key = article.ArticleKey.Key };
                request.SortType = dateSort;
                request.FilterType = threadFilter;
                request.ItemsPerPage = maxNumberOfCommentsPerArticle;
                request.OneBasedOnPage = 1;
                requests.AddRequest(request);
            }

            ResponseBatch response = SDKRequests.GetBatchResponse(requests, pluckHost, false);
            if (response != null)
            {
                List<Comment> comments = new List<Comment>();
                foreach (ResponseEnvelope envelope in response.Envelopes)
                {
                    IResponse envResponse = envelope.GetResponse();
                    if (envResponse.ResponseStatus.StatusCode == Pluck.SiteLife.SDK.Models.System.ResponseStatusCode.OK)
                    {
                        if (envResponse is CommentsPageResponse)
                        {
                            foreach (Comment comment in ((CommentsPageResponse)envResponse).Items)
                            {
                                totalComments++;
                                comments.Add(comment);
                            }
                        }
                    }
                }
                return comments;
            }

            return null;
        }
    }
}
