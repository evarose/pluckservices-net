﻿using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.Common;
using Pluck.SiteLife.SDK.Models.Discovery;
using Pluck.SiteLife.SDK.Models.External;
using Pluck.SiteLife.SDK.Models.System;
using Pluck.SiteLife.SDK.Models.Users;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.Common;
using Pluck.SiteLife.SDK.Responses.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace PluckIL.Helpers
{
    public class PolicyHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        public static Array GetActionTypes()
        {
            ContentPolicyActionEnum[] actionTypes = (ContentPolicyActionEnum[])Enum.GetValues(typeof(ContentPolicyActionEnum));
            return actionTypes.OrderBy(a => a.ToString()).ToArray();
        }

        public static Array GetContentPolicy(string baseKey, string actionType, string userTier, out int totalItems, out string objectType, out string pluckStatus)
        {
            totalItems = 0;
            objectType = String.Empty;
            pluckStatus = String.Empty;

            dynamic obj;
            string message;
            if (!PluckConfig.CheckPluckStatus(out obj, out message))
            {
                pluckStatus = message;
                return null;
            }

            if (!String.IsNullOrEmpty(baseKey))
            {
                ContentPolicyTraceRequest request = new ContentPolicyTraceRequest();
                request.TargetKey = new ExternalResourceKey { Key = baseKey };
                request.ActionType = GetActionTypes().Cast<ContentPolicyActionEnum>().Where(a => a.ToString() == actionType).FirstOrDefault();
                request.UserTier = UserHelpers.GetUserTiers().Cast<UserTier>().Where(u => u.ToString() == userTier).FirstOrDefault();

                RequestBatch requests = new RequestBatch();
                requests.AddRequest(request);

                IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, true);
                if (response != null)
                {
                    ResponseStatus statusResponse = response.ResponseStatus;
                    if (statusResponse.StatusCode.Equals(ResponseStatusCode.OK))
                    {
                        ContentPolicyTraceResponse traceResponse = (ContentPolicyTraceResponse)response;
                        List<ContentPolicyTraceEntry> traceEntry = new List<ContentPolicyTraceEntry>();
                        traceEntry.AddRange(traceResponse.Trace);
                        return traceEntry.ToArray();
                    }
                    else
                    {
                        pluckStatus = CustomMessage.PluckResponseError + statusResponse.StatusCode.ToString();
                    }
                }
            }

            return null;
        }

        public static Array GetDiscoveryContentPolicy(string category, string section, string actionType, string userTier, out int totalItems, out string objectType, out string pluckStatus)
        {
            totalItems = 0;
            objectType = String.Empty;
            pluckStatus = String.Empty;

            dynamic obj;
            string message;
            if (!PluckConfig.CheckPluckStatus(out obj, out message))
            {
                pluckStatus = message;
                return null;
            }

            if (!String.IsNullOrEmpty(category) || !String.IsNullOrEmpty(section))
            {
                DiscoveryContentPolicyTraceRequest request = new DiscoveryContentPolicyTraceRequest();
                List<DiscoveryCategory> categories = new List<DiscoveryCategory>();
                categories.Add(new DiscoveryCategory { Name = category });
                request.Categories = categories.ToArray();
                request.Section = new DiscoverySection { Name = section };
                request.ActionType = GetActionTypes().Cast<ContentPolicyActionEnum>().Where(a => a.ToString() == actionType).FirstOrDefault();
                request.UserTier = UserHelpers.GetUserTiers().Cast<UserTier>().Where(u => u.ToString() == userTier).FirstOrDefault();

                RequestBatch requests = new RequestBatch();
                requests.AddRequest(request);

                IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, true);
                if (response != null)
                {
                    ResponseStatus statusResponse = response.ResponseStatus;
                    if (statusResponse.StatusCode.Equals(ResponseStatusCode.OK))
                    {
                        DiscoveryContentPolicyTraceResponse traceResponse = (DiscoveryContentPolicyTraceResponse)response;
                        List<ContentPolicyTraceEntry> traceEntry = new List<ContentPolicyTraceEntry>();
                        traceEntry.AddRange(traceResponse.Trace);
                        return traceEntry.ToArray();
                    }
                    else
                    {
                        pluckStatus = CustomMessage.PluckResponseError + statusResponse.StatusCode.ToString();
                    }
                }
            }

            return null;
        }
    }
}
