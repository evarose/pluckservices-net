﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.Forums;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.Common;
using Pluck.SiteLife.SDK.Responses.Common;

namespace PluckIL.Helpers
{
    public class SEOHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        public static string ResolveSEOToken(string SEOToken)
        {
            string plckForumString = String.Empty;

            ResolveSEOTokenRequest request = new ResolveSEOTokenRequest();
            request.SEOToken = SEOToken;

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);

            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, false);
            if (response != null)
            {
                if (((ResolveSEOTokenResponse)response).Item != null)
                {
                    switch (((ResolveSEOTokenResponse)response).Item.GetType().Name)
                    {
                        case "ForumCategory":
                            plckForumString = "plckForumPage=ForumCategory&plckCategoryId=" + HttpUtility.UrlEncode(((ForumCategory)((ResolveSEOTokenResponse)response).Item).ForumCategoryKey.Key);
                            break;

                        case "Forum":
                            plckForumString = "plckForumPage=Forum&plckForumId=" + HttpUtility.UrlEncode(((Forum)((ResolveSEOTokenResponse)response).Item).ForumKey.Key);
                            break;

                        case "ForumDiscussion":
                            plckForumString = "plckForumPage=ForumDiscussion&plckDiscussionId=" + HttpUtility.UrlEncode(((ForumDiscussion)((ResolveSEOTokenResponse)response).Item).ForumDiscussionKey.Key);
                            break;
                    }
                }
            }

            return plckForumString;
        }
    }
}
