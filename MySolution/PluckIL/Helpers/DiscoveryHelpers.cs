﻿using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.Discovery;
using Pluck.SiteLife.SDK.Models.External;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.Discovery;
using Pluck.SiteLife.SDK.Responses.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace PluckIL.Helpers
{
    public class DiscoveryHelpers
    {
        private static string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];

        public static IDaapiModel[] DiscoverContent(ViewModels.DiscoverContentModel discoveryRequest, out int totalItems)
        {
            totalItems = 0;

            DiscoverContentActionRequest request = new DiscoverContentActionRequest();
            request.Activity = discoveryRequest.DiscoveryActivity;
            request.Type = discoveryRequest.ContentType;
            request.Sections = discoveryRequest.DiscoverySections;
            request.Categories = discoveryRequest.DiscoveryCategories;
            request.Age = discoveryRequest.Age;
            request.MaximumNumberOfDiscoveries = discoveryRequest.MaximumNumberOfDiscoveries;

            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);

            IResponse response = SDKRequests.GetSingleResponse(requests, pluckHost, false);
            if (response != null)
            {
                if (response.ResponseStatus.StatusCode == Pluck.SiteLife.SDK.Models.System.ResponseStatusCode.OK)
                {
                    DiscoverContentResponse discoResponse = (DiscoverContentResponse)response;
                    return discoResponse.DiscoveredContent;
                }
            }

            return null;
        }
    }
}
