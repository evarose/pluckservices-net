﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Pluck.SiteLife.SDK;
using Pluck.SiteLife.SDK.Responses;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Security;
using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Models.System;
using System.Reflection;

namespace PluckIL
{
    public class SDKRequests
    {
        private static string sharedSecret = WebConfigurationManager.AppSettings["PluckSharedSecret"];
        private static string editorKey = WebConfigurationManager.AppSettings["PluckEditorKey"];
        private static string editorName = WebConfigurationManager.AppSettings["PluckEditorName"];
        private static string editorEmail = WebConfigurationManager.AppSettings["PluckEditorEmail"];

        private static Log4NetAdapter logAdapter = new Log4NetAdapter(typeof(SDKRequests));

        /// <summary>
        /// Gets the batch response from Pluck.
        /// </summary>
        /// <param name="requests">Batch requests.</param>
        /// <param name="pluckHost">Pluck host.</param>
        /// <param name="isEditorRequest">Flags whether this is Editor only request</param>
        /// <returns></returns>
        public static ResponseBatch GetBatchResponse(RequestBatch requests, string pluckHost, bool isEditorRequest)
        {
            PluckService service = new PluckService(pluckHost + "/ver1.0/daapi2.api");
            ResponseBatch responseBatch = null;

            //If this request is for Editor only level, then set editor token here, otherwise, set token to null (which means the call will be sent as a current logged on user
            UserAuthenticationToken authToken = null;
            
            if (isEditorRequest)
            {
                authToken = new UserAuthenticationToken(editorKey, editorName, editorEmail, sharedSecret);
            }
            else
            {
                authToken = new UserAuthenticationToken(SSO.GetATCookieValue("u"), SSO.GetATCookieValue("a"), SSO.GetATCookieValue("e"), sharedSecret);
            }

            Exception sendRequestException;
            StringBuilder sb = new StringBuilder();

            responseBatch = service.SendRequest(requests, authToken, out sendRequestException);

            if (responseBatch != null)
            {
                return responseBatch;
            }

            sb.Append(" - " + sendRequestException.Message + Environment.NewLine);
            sb.Append(sendRequestException.StackTrace + Environment.NewLine);
            logAdapter.LogError(MethodBase.GetCurrentMethod(), sb.ToString());
            return null;
        }

        /// <summary>
        /// Gets the single response from Pluck
        /// </summary>
        /// <param name="requests">Batch requests.</param>
        /// <param name="pluckHost">Pluck host.</param>
        /// <param name="isEditorRequest">Flags whether this is Editor only request</param>
        /// <returns></returns>
        public static IResponse GetSingleResponse(RequestBatch requests, string pluckHost, bool isEditorRequest)
        {
            PluckService service = new PluckService(pluckHost + "/ver1.0/daapi2.api");
            ResponseBatch responseBatch = null;

            //If this request is for Editor only level, then set editor token here, otherwise, set token to null (which means the call will be sent as a current logged on user
            UserAuthenticationToken authToken = null;
            if (isEditorRequest)
            {
                authToken = new UserAuthenticationToken(editorKey, editorName, editorEmail, sharedSecret);
            }
            else
            {
                authToken = new UserAuthenticationToken(SSO.GetATCookieValue("u"), SSO.GetATCookieValue("a"), SSO.GetATCookieValue("e"), sharedSecret);
            }

            Exception sendRequestException;
            StringBuilder sb = new StringBuilder();

            responseBatch = service.SendRequest(requests, authToken, out sendRequestException);

            if (responseBatch != null)
            {
                IResponse response = responseBatch.Envelopes[0].GetResponse();
                if (response.ResponseStatus.StatusCode == ResponseStatusCode.OK)
                {
                    return response;
                }
                sb.Append(response.ResponseStatus.StatusCode + Environment.NewLine);
            }

            if (sendRequestException != null)
            {
                sb.Append(sendRequestException.Message + Environment.NewLine);
                sb.Append(sendRequestException.StackTrace + Environment.NewLine);
            }
            logAdapter.LogError(MethodBase.GetCurrentMethod(), sb.ToString());
            return null;
        }
    }
}
