﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using PluckIL.Helpers;

namespace PluckIL
{
    public class RestRequest
    {
        public static string GetRestResponse(PluckApp appName, Dictionary<string,string> parameters, bool seoEnabled, string seoRootUrl, out string restUrl)
        {
            HttpContextWrapper httpContext = new HttpContextWrapper(HttpContext.Current);
            restUrl = String.Empty;
            string message;
            dynamic color;

            if (!PluckConfig.CheckPluckStatus(out color, out message))
            {
                restUrl = message;
                return message;
            }

            StringBuilder sbParam = new StringBuilder();
            if (parameters.Count > 0)
            {
                foreach (KeyValuePair<string, string> param in parameters)
                {
                    sbParam.Append("&" + param.Key + "=" + HttpUtility.UrlEncode(param.Value));
                }
            }

            if (seoEnabled)
            {
                if (seoRootUrl.Last().Equals('/'))
                    seoRootUrl = seoRootUrl.Remove(seoRootUrl.Length - 1);

                string currentUrl = httpContext.Request.Url.AbsoluteUri;

                if (currentUrl != seoRootUrl)
                {
                    string SEOToken = currentUrl.Remove(0, seoRootUrl.Length);
                    sbParam.Append("&" + SEOHelpers.ResolveSEOToken(SEOToken));
                }
            }
            
            restUrl = String.Format("{0}/ver1.0/pluck/{1}?clientUrl={2}{3}",
                    WebConfigurationManager.AppSettings["PluckHost"],
                    GetEnumValue(appName),
                    HttpUtility.UrlEncode(httpContext.Request.Url.ToString()),
                    sbParam.ToString()
                    );

            return GetPage(restUrl);
        }

        private static string GetEnumValue(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DefaultValueAttribute[] attributes =
                (DefaultValueAttribute[])fi.GetCustomAttributes(typeof(DefaultValueAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Value.ToString();
            else
                return value.ToString();
        }

        private static string GetPage(string restUrl)
        {
            string result = string.Empty;
            try
            {
                HttpWebRequest request = WebRequest.Create(new Uri(restUrl)) as HttpWebRequest;
                request.Headers.Add("Accept-Charset", "utf-8");

                // Forward on the SSO Cookie
                AttachCookie(request, "at");

                // Forward on the anonID cookie. The anonId cookie is used prevent the user from performing the same action repeatedly. For instance, rating an article multiple times while anonymous.
                AttachCookie(request, "anonId");

                // Forward on the Debug Strings Cookie. Useful for viewing externalized strings in debug mode for customization.  Optional..
                AttachCookie(request, "plckDebugStrings");

                // Sticky cookies. Very important!
                AttachCookie(request, "BigIP");
                AttachCookie(request, "SiteLifeHost");

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    result = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                throw ex;
            }
            finally
            {
                //logAdapter.Log("REST call completed.");
            }
            return result;
        }

        private static void AttachCookie(HttpWebRequest request, string cookie)
        {
            if (HttpContext.Current.Request.Cookies[cookie] != null)
            {
                if (request.CookieContainer == null) request.CookieContainer = new CookieContainer();
                Cookie netCookie = new Cookie(cookie, HttpContext.Current.Request.Cookies[cookie].Value);

                netCookie.Domain = WebConfigurationManager.AppSettings["PluckDomain"];
                request.CookieContainer.Add(netCookie);
            }
        }
    }
}
