﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Pluck.SiteLife.SDK.Interfaces;
using Pluck.SiteLife.SDK.Requests;
using Pluck.SiteLife.SDK.Requests.Common;

namespace PluckIL
{
    public static class PluckConfig
    {
        public static bool ATCookieIsHttpOnly { get; set; }

        private static Log4NetAdapter logAdapter = new Log4NetAdapter(typeof(PluckConfig));

        static PluckConfig()
        {
            ATCookieIsHttpOnly = false;
        }

        public static bool CheckPluckStatus(out dynamic outObject, out string message)
        {
            outObject = null;
            message = String.Empty;

            if (!PluckConfig.IsPluckConfigured())
            {
                outObject = Color.Red;
                message = CustomMessage.PluckNotConfigured;
                return false;
            }

            if (!PluckConfig.IsPluckEnabled())
            {
                outObject = Color.Red;
                message = CustomMessage.PluckDisabled;
                return false;
            }

            if (!PluckConfig.IsPluckAvailable())
            {
                outObject = Color.Red;
                message = CustomMessage.PluckUnavailable;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines whether Pluck is configured in Web.Config file
        /// </summary>
        /// <returns></returns>
        public static bool IsPluckConfigured()
        {
            if (WebConfigurationManager.AppSettings.Count == 0)
                return false;
            else
            {
                Dictionary<string, string> pluckConfig = new Dictionary<string, string>();
                foreach (string key in WebConfigurationManager.AppSettings.AllKeys)
                {
                    if (key.Contains("Pluck"))
                        pluckConfig.Add(key, WebConfigurationManager.AppSettings[key]);
                }

                if (pluckConfig.Count == 0)
                    return false;

                foreach (KeyValuePair<string, string> config in pluckConfig)
                {
                    if (String.IsNullOrEmpty(config.Value))
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Determines whether Pluck is enabled/disabled
        /// </summary>
        /// <returns></returns>
        public static bool IsPluckEnabled()
        {
            if (WebConfigurationManager.AppSettings["PluckEnabled"].ToLower() == "true")
                return true;
            return false;
        }

        /// <summary>
        /// Determines whether Pluck is available
        /// </summary>
        /// <returns></returns>
        public static bool IsPluckAvailable()
        {
            //Make a simple SDK request to check whether Pluck is available or not
            MemberSitesRequest request = new MemberSitesRequest();
            RequestBatch requests = new RequestBatch();
            requests.AddRequest(request);

            IResponse response = SDKRequests.GetSingleResponse(requests, WebConfigurationManager.AppSettings["PluckHost"], true);
            if (response != null)
                return true;
            return false;
        }

        /// <summary>
        /// Determines whether PluckIL logging is enabled.
        /// </summary>
        /// <returns></returns>
        public static bool IsLoggingEnabled()
        {
            if (WebConfigurationManager.AppSettings["PluckILLoggingEnabled"].ToLower() == "true")
                return true;
            return false;
        }

        /// <summary>
        /// Determines whether Log4Net is configured or not.
        /// </summary>
        /// <returns></returns>
        public static bool IsLoggingConfigured()
        {
            return log4net.LogManager.GetRepository().Configured;
        }

        /// <summary>
        /// Enables Pluck
        /// </summary>
        public static void EnablePluck()
        {
            logAdapter.LogInfo(MethodBase.GetCurrentMethod(), CustomMessage.PluckEnabled);
            WebConfigurationManager.AppSettings["PluckEnabled"] = "true";
        }

        /// <summary>
        /// Disables Pluck
        /// </summary>
        public static void DisablePluck()
        {
            logAdapter.LogInfo(MethodBase.GetCurrentMethod(), CustomMessage.PluckDisabled);
            WebConfigurationManager.AppSettings["PluckEnabled"] = "false";
        }

        /// <summary>
        /// Switches Pluck Url to make connection available/un-available
        /// </summary>
        /// <param name="url">The Url.</param>
        public static void SwitchPluckUrl(string url)
        {
            logAdapter.LogInfo(MethodBase.GetCurrentMethod(), CustomMessage.PluckUrlSwitched + " to " + url);
            WebConfigurationManager.AppSettings["PluckHost"] = url;
        }

        /// <summary>
        /// Switches the logging.
        /// </summary>
        /// <param name="flag">true or false</param>
        public static void SwitchLogging(bool flag)
        {
            WebConfigurationManager.AppSettings["PluckILLoggingEnabled"] = flag.ToString();
        }        
    }
}
