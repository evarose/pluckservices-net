﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace PluckIL
{
    public class Log4NetAdapter : ILogger
    {
        private readonly log4net.ILog _log;

        public Log4NetAdapter(Type type)
        {
            XmlConfigurator.Configure();
             _log = LogManager.GetLogger(type);
        }

        public void LogInfo(MethodBase methodBase, string message)
        {
            if (PluckConfig.IsLoggingEnabled())
                _log.Info("[MethodName: " + methodBase.Name + "] - " + message);
        }

        public void LogError(MethodBase methodBase, string message)
        {
            if (PluckConfig.IsLoggingEnabled())
                _log.Error("[MethodName: " + methodBase.Name + "] - " + message);
        }

        public void LogDebug(MethodBase methodBase, string message)
        {
            if (PluckConfig.IsLoggingEnabled())
                _log.Debug("[MethodName: " + methodBase.Name + "] - " + message);
        }
    }
}
