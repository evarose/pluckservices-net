﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PluckIL
{
    public interface ILogger
    {
        void LogInfo(MethodBase methodBase, string message);
        void LogError(MethodBase methodBase, string message);
        void LogDebug(MethodBase methodBase, string messsage);
    }
}
