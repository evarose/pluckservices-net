﻿using Pluck.SiteLife.SDK.Models.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluckIL
{
    public class ViewModels
    {
        public class ArticleModel
        {
            public string ArticleKey { get; set; }
            public string ArticleTitle { get; set; }
            public string ArticleUrl { get; set; }
            public string Categories { get; set; }
            public string Section { get; set; }
            public bool IsReview { get; set; }
            public uint NumberOfReviews { get; set; }
            public uint NumberOfReviewComments { get; set; }
            public uint NumberOfComments { get; set; }
            public uint NumberOfTopLevelComments { get; set; }
            public string ReviewItemState { get; set; }
            public string SiteOfOrigin { get; set; }
            public string SourceSiteName { get; set; }
            public string SourceSiteKey { get; set; }
        }

        public class CommentModel
        {
            public string CommentKey { get; set; }
            public string CommentBody { get; set; }
            public DateTime PostedAtTime { get; set; }
            public int NumberOfReplies { get; set; }
            public string ThreadPath { get; set; }
            public int ThreadDepth { get; set; }
            public string CommentSection { get; set; }
            public string CommentCategories { get; set; }
            public string Tags { get; set; }
            public string AuthorKey { get; set; }
            public string AuthorName { get; set; }
            public string AuthorUserTier { get; set; }
            public string AuthorAvatarUrl { get; set; }
            public bool IsExpertAnswer { get; set; }
            public string ArticleTitle { get; set; }
            public string ArticleKey { get; set; }
            public string ArticleSection { get; set; }
            public string ArticleCategories { get; set; }
            public string SiteOfOrigin { get; set; }
        }

        public class DiscoverContentModel
        {
            public DiscoveryActivity DiscoveryActivity { get; set; }
            public ContentType ContentType { get; set; }
            public DiscoverySection[] DiscoverySections { get; set; }
            public DiscoveryCategory[] DiscoveryCategories { get; set; }
            public int Age { get; set; }
            public int MaximumNumberOfDiscoveries { get; set; }
        }

        public class PointsAndBadgingActivityModel
        {
            public DateTime ActivityDate { get; set; }
            public string ActivityName { get; set; }
            public string CurrencyType { get; set; }
            public int Points { get; set; }
            public string ActivityTags { get; set; }
        }

        public class QADiscoveryModel
        {
            public string QuestionKey { get; set; }
            public string QuestionTitle { get; set; }
            public string QuestionBody { get; set; }
            public string QuestionerKey { get; set; }
            public string QuestionerName { get; set; }
            public string QuestionerAvatarUrl { get; set; }
            public DateTime QuestionDatePosted { get; set; }
            public string QuestionCategories { get; set; }
            public string AnswerKey { get; set; }
            public string AnswerBody { get; set; }
            public string AnswererKey { get; set; }
            public string AnswererName { get; set; }
            public string AnswererAvatarUrl { get; set; }
            public DateTime AnswerDatePosted { get; set; }
            public int NumberOfReplies { get; set; }
        }
    }
}
