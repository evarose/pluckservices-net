﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluckIL
{
    public class CustomMessage
    {
        public const string PluckNotConfigured = "Pluck is not configured. Please check your Web.Config file";
        public const string PluckEnabled = "Pluck is enabled";
        public const string PluckDisabled = "Pluck is disabled";
        public const string PluckUnavailable = "Pluck is currently unavailable";
        public const string PluckUrlSwitched = "Pluck Url switched";
        public const string LoggingEnabled = "PluckIL logging enabled";
        public const string LoggingDisabled = "PluckIL logging disabled";
        public const string LoggingNotConfigured = "PluckIL uses Log4net for its logging feature. Please check your Web.Config file for its configuration.";
        public const string ResultsNotFound = "No results returned";
        public const string SendRequestError = "Error on Pluck SDK SendRequest";
        public const string ArticleNotFound = "Article not found";
        public const string ArticleUpdated = "Article successfully updated";
        public const string PluckResponseError = "Error on Pluck Response. Response status code is ";
        public const string PluckNoResponse = "Null response from Pluck";
        public const string UserNotFound = "User not found";
        public const string PointsAwarded = "Points awarded to user.";
        public const string BadgingEventCompleted = "Badging event completed.";
    }
}
