﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluckIL
{
    public enum PluckApp
    {
        [DefaultValue("")]
        NotDefined = 0,
        [DefaultValue("comments.app")]
        Comments = 1,
        [DefaultValue("discovery.app")]
        Discovery = 2,
        [DefaultValue("forums.app")]
        Forums = 3,
        [DefaultValue("groups.app")]
        Groups = 4,
        [DefaultValue("itemRatings.app")]
        ItemRatings = 5,
        [DefaultValue("leaderboards.app")]
        Leaderboards = 6,
        [DefaultValue("user/persona.app")]
        Persona = 7,
        [DefaultValue("polls.app")]
        Polls = 8,
        [DefaultValue("presence.app")]
        Presence = 9,
        [DefaultValue("privateMessages.app")]
        PrivateMessages = 10,
        [DefaultValue("publicBlogs.app")]
        Blogs = 11,
        [DefaultValue("publicPhotos.app")]
        Photos = 12,
        [DefaultValue("publicVideos.app")]
        Videos = 13,
        [DefaultValue("reactions/recommend.app")]
        Recommend = 14,
        [DefaultValue("reviews/list.app")]
        ReviewsList = 15,
        [DefaultValue("reviews/mostHelpful.app")]
        ReviewsMostHelpful = 16,
        [DefaultValue("reviews/recent.app")]
        ReviewsRecent = 17,
        [DefaultValue("reviews/rollup.app")]
        ReviewsRollup = 18,
        [DefaultValue("reviews/topRated.app")]
        ReviewsTopRated = 19,
        [DefaultValue("util/share.app")]
        Share = 20,
        [DefaultValue("login/withFacebook.app")]
        SocialSignOn = 21,
        [DefaultValue("social/syndicateOnDemand.app")]
        SyndicateOnDemand = 22,
        [DefaultValue("social/syndication/messages.app")]
        Syndication = 23,
        [DefaultValue("user/seoblog.app")]
        UserSEOBlog = 24,
        [DefaultValue("userReactions.app")]
        Reactions = 25,
    }
}
