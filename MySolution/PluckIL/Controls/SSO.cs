﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Security.Cryptography;
using System.Configuration;
using log4net;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PluckIL
{
    public class SSO : Button
    {
        public string UserKey { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public bool HttpOnly { get; set; }
        private static Log4NetAdapter logAdapter = new Log4NetAdapter(typeof(SSO));
        private static string sharedSecret = ConfigurationManager.AppSettings["PluckSharedSecret"];
        private static string pluckDomain = ConfigurationManager.AppSettings["PluckDomain"];

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            string message;
            dynamic color;
            if (!PluckConfig.CheckPluckStatus(out color, out message))
            {
                Text = message;
                Enabled = false;
            }
            else
            {
                if (String.IsNullOrEmpty(Text))
                    Text = "SSO Login to Pluck";
                Click += new EventHandler(OnClick_Login);
            }
        }

        void OnClick_Login(object sender, EventArgs e)
        {
            // Authenticate in Pluck on button click
            if ((!String.IsNullOrEmpty(UserKey)) && (!String.IsNullOrEmpty(DisplayName)) && (!String.IsNullOrEmpty(Email)))
            {
                string message;
                if (Login(UserKey, DisplayName, Email, HttpOnly, out message))
                {
                    HttpContextWrapper httpContext = new HttpContextWrapper(HttpContext.Current);
                    httpContext.Response.Redirect(httpContext.Request.Url.AbsoluteUri);
                }
            }
        }

        /// <summary>
        /// Login the user by creating "at" cookie
        /// </summary>
        /// <param name="userKey">The user key.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="email">The email.</param>
        /// <param name="isHttpOnly">Sets "at" cookie as HttpOnly</param>
        /// <returns></returns>
        public static bool Login(string userKey, string displayName, string email, bool isHttpOnly, out string message)
        {
            dynamic color;
            if (!PluckConfig.CheckPluckStatus(out color, out message))
                return false;

            HttpContextWrapper httpContext = new HttpContextWrapper(HttpContext.Current);

            string timeStamp = "1";

            // Concatenate userKey.displayName.timeStamp.email.sharedSecret
            string cookieString = userKey.Trim() + "." + displayName.Trim() + "." + timeStamp + "." + email.Trim() + "." + sharedSecret.Trim();
            string hashedValue = CalculateMD5Hash(cookieString);

            // Build un-coded value of cookie string
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("u={0}", userKey.Trim());
            sb.AppendFormat("&a={0}", displayName.Trim());
            sb.AppendFormat("&e={0}", email.Trim());
            sb.AppendFormat("&t={0}", timeStamp.Trim());
            sb.AppendFormat("&h={0}", hashedValue.Trim().ToLower());
            string unencodedValue = sb.ToString();

            // URL-encode the string
            string encodedValue = HttpUtility.UrlEncode(unencodedValue).Replace("%20", "+");

            // Create "at" cookie
            HttpCookie cookie = new HttpCookie("at");
            cookie.Domain = pluckDomain;
            cookie.Path = "/";
            cookie.Value = encodedValue;
            cookie.HttpOnly = isHttpOnly;

            // Set global variable
            PluckConfig.ATCookieIsHttpOnly = isHttpOnly;

            // Drop "at" cookie
            httpContext.Response.Cookies.Add(cookie);

            // Logging
            StringBuilder sbLog = new StringBuilder();
            sbLog.Append("AT cookie created." + Environment.NewLine);
            sbLog.Append("\tUn-encoded value: [" + unencodedValue + "]" + Environment.NewLine);
            sbLog.Append("\tEncoded value: [" + encodedValue + "]" + Environment.NewLine);
            logAdapter.LogInfo(MethodBase.GetCurrentMethod(), sbLog.ToString());

            return true;
        }

        /// <summary>
        /// Logout the user by destroying "at" cookie
        /// </summary>
        public static void Logout()
        {
            var httpContext = new HttpContextWrapper(HttpContext.Current);
            if (httpContext.Request.Cookies["at"] != null)
            {
                HttpCookie cookie = new HttpCookie("at");
                cookie.Domain = pluckDomain;
                cookie.Path = "/";
                cookie.Expires = DateTime.Now.AddDays(-1d);
                httpContext.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Gets the "at" cookie value by passing one single parameter
        /// </summary>
        /// <param name="keyName">Parameter name</param>
        /// <returns></returns>
        public static string GetATCookieValue(string keyName)
        {
            string returnValue = String.Empty;
            HttpCookie AT = HttpContext.Current.Request.Cookies["at"];
            if (AT != null)
            {
                string ATCookie = HttpUtility.UrlDecode(AT.Value);
                foreach (string keyValuePair in ATCookie.Split('&'))
                {
                    if (keyValuePair.Contains(keyName + "="))
                    {
                        returnValue = keyValuePair.Split('=')[1].ToString().Replace("+", " ").Replace("%2b", " ");
                        break;
                    }
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Calculates the MD5 hash
        /// </summary>
        /// <param name="input">String input</param>
        /// <returns></returns>
        private static string CalculateMD5Hash(string input)
        {
            // Step 1, calculate MD5 hash from input
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(input);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);

            // Step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encodedBytes.Length; i++)
            {
                sb.Append(encodedBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
