﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;

namespace PluckIL
{
    public class RestLabel : Label
    {
        public PluckApp AppName { get; set; }
        public string Parameters { get; set; }
        public bool RefreshPage { get; set; }
        //public ForumType ForumType { get; set; }
        //public ForumPage ForumPage { get; set; }
        //public string ForumRootUrl { get; set; }
        //public string CategoryId { get; set; }
        //public string ForumId { get; set; }
        //public string DiscussionId { get; set; }
        

        private static Log4NetAdapter logAdapter = new Log4NetAdapter(typeof(RestLabel));

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Text = GetRestResponse();
            //PropertyInfo[] propInfos = this.GetType().GetProperties();
            //foreach (PropertyInfo propInfo in propInfos)
            //{
            //    Text += propInfo.Name;
            //}
        }

        public static string GetRestUrl(PluckApp pluckApp, string parameters)
        {
            HttpContextWrapper httpContext = new HttpContextWrapper(HttpContext.Current);

            StringBuilder sbParam = new StringBuilder();
            if (parameters.Length > 0)
            {
                string[] pluckParams = parameters.Split('&');
                for (int i = 0; i < pluckParams.Length; i++)
                {
                    if (!String.IsNullOrEmpty(pluckParams[i]))
                    {
                        string[] keyValuePair = pluckParams[i].Split('=');
                        sbParam.Append(keyValuePair[0] + "=" + HttpUtility.UrlEncode(keyValuePair[1]));
                        if (i != pluckParams.Length - 1)
                            sbParam.Append("&");
                    }
                }
            }

            // Construct REST Url here and return
            return String.Format("{0}/ver1.0/pluck/{1}?clientUrl={2}{3}",
                    WebConfigurationManager.AppSettings["PluckHost"],
                    GetEnumValue(pluckApp),
                    HttpUtility.UrlEncode(httpContext.Request.Url.ToString()),
                    (!String.IsNullOrEmpty(parameters)) ? "&" + sbParam.ToString() : String.Empty
                    );
        }

        private string GetRestResponse()
        {
            string message;
            dynamic color;
            if (!PluckConfig.CheckPluckStatus(out color, out message))
            {
                ForeColor = color;
                return message;
            }

            if (RefreshPage)
                Parameters += "&plckRefreshPage=true";

            string restUrl = GetRestUrl(AppName, Parameters);

            //StringBuilder sbLog = new StringBuilder();
            //sbLog.Append(Environment.NewLine);
            //sbLog.Append("\tHost: [" + pluckHost + "]" + Environment.NewLine);
            //sbLog.Append("\tREST app: [" + RESTapp + "]" + Environment.NewLine);
            //sbLog.Append("\tClientUrl: [" + currentUrl + "]" + Environment.NewLine);
            //if (parameters.Length > 0)
            //{
            //    foreach (string param in parameters.Split('&'))
            //    {
            //        if (!String.IsNullOrEmpty(param))
            //        {
            //            sbLog.Append("\t" + param.Split('=')[0].ToString() + ": [" + HttpUtility.UrlDecode(param.Split('=')[1].ToString()) + "]" + Environment.NewLine);
            //        }
            //    }
            //}
            //logAdapter.LogInfo(MethodBase.GetCurrentMethod(), restUrl + sbLog.ToString());

            return GetPage(restUrl);
        }

        private static string GetEnumValue(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DefaultValueAttribute[] attributes =
                (DefaultValueAttribute[])fi.GetCustomAttributes(typeof(DefaultValueAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Value.ToString();
            else
                return value.ToString();
        }

        private static string GetPage(string restUrl)
        {
            string result = string.Empty;
            try
            {
                // Create the web request  
                HttpWebRequest request = WebRequest.Create(new Uri(restUrl)) as HttpWebRequest;
                request.Headers.Add("Accept-Charset", "utf-8");

                // Forward on the SSO Cookie
                AttachCookie(request, "at");

                // Forward on the anonID cookie. The anonId cookie is used prevent the user from performing the same action repeatedly. For instance, rating an article multiple times while anonymous.
                AttachCookie(request, "anonId");

                // Forward on the Debug Strings Cookie. Useful for viewing externalized strings in debug mode for customization.  Optional..
                AttachCookie(request, "plckDebugStrings");

                // Sticky cookies. Very important!
                AttachCookie(request, "BigIP");
                AttachCookie(request, "SiteLifeHost");

                // Get response  
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    // Get the response stream  
                    StreamReader reader = new StreamReader(response.GetResponseStream());

                    // Read the whole contents and return as a string  
                    result = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                throw ex;
            }
            finally
            {
                //logAdapter.Log("REST call completed.");
            }
            return result;
        }

        private static void AttachCookie(HttpWebRequest request, string cookie)
        {
            if (HttpContext.Current.Request.Cookies[cookie] != null)
            {
                if (request.CookieContainer == null) request.CookieContainer = new CookieContainer();
                Cookie netCookie = new Cookie(cookie, HttpContext.Current.Request.Cookies[cookie].Value);

                netCookie.Domain = WebConfigurationManager.AppSettings["PluckDomain"];
                request.CookieContainer.Add(netCookie);
            }
        }
    }
}
