﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PluckIL
{
    [
    ParseChildren(true, "WidgetParameters"),
    ToolboxData("<{0}:Rest runat=\"server\"></{0}:Rest")
    ]
    public class Rest : WebControl
    {
        private static Log4NetAdapter logAdapter = new Log4NetAdapter(typeof(Rest));

        public Rest()
        {
            this.WidgetParameters = new List<WidgetParameter>();
        }

        [
        Bindable(true),
        Category("Appearance"),
        DefaultValue(PluckApp.NotDefined),
        Description("Pluck application name")
        ]
        public virtual PluckApp AppName { get; set; }

        [
        Bindable(true),
        Category("Appearance"),
        DefaultValue(false),
        Description("Return REST Url if true")
        ]
        public bool ShowRestUrl { get; set; }

        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public List<WidgetParameter> WidgetParameters { get; set; }

        [DefaultValue(false)]
        public bool SEOEnabled { get; set; }
        [DefaultValue("")]
        public string SEORootUrl { get; set; }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            String restUrl = String.Empty;
            Dictionary<string, string> parameters = new Dictionary<string,string>();
            foreach (WidgetParameter param in WidgetParameters)
            {
                parameters.Add(param.Key, param.Value);
            }

            String restWidget = RestRequest.GetRestResponse(AppName, parameters, SEOEnabled, SEORootUrl, out restUrl);

            if (ShowRestUrl)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Pre);
                writer.WriteEncodedText(restUrl);
                writer.RenderEndTag();
            }

            writer.Write(restWidget);
        }
    }

    public class WidgetParameter
    {
        public String ID { get; set; }
        public String Key { get; set; }
        public String Value { get; set; }
    }
}
