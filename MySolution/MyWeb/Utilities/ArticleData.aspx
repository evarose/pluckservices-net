﻿<%@ Page Title="Article Utility" Language="C#" MasterPageFile="~/Masters/Utilities.master" AutoEventWireup="true" CodeBehind="ArticleData.aspx.cs" Inherits="MyWeb.Utilities.ArticleData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SDKToolContent" runat="server">

    <h4>Article Utility</h4>

    <p>The article utility uses PluckIL.ArticleHelpers (which calls up a number of Pluck SDKs). When you get the result, you are able to edit the article meta data, which calls up Pluck SDK UpdateArticleActionRequest.</p>

    <table border="0" class="sdk-table">
        <tr>
            <td>Article key:</td>
            <td>
                <asp:TextBox ID="txtArticleKey" runat="server" Text="myTestArticle.html"></asp:TextBox></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnGetArticle" runat="server" Text="Get Article" OnClick="GetArticle" CssClass="btn btn-primary btn-large" /></td>
        </tr>
    </table>

    <asp:PlaceHolder ID="plcData" runat="server" Visible="false">

        <hr style="border: 0; border-bottom: 1px dotted #808080; width: 100%" />

        <asp:PlaceHolder ID="plcPluckStatus" runat="server" Visible="false">
            <pre class="red"><asp:Label ID="lblPluckStatus" runat="server"></asp:Label></pre>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcResults" runat="server" Visible="false">
            <pre><asp:Label ID="lblResult" runat="server"></asp:Label></pre>
        </asp:PlaceHolder>

        <asp:DetailsView ID="dvArticle" runat="server" AutoGenerateRows="false" CellPadding="4" AutoGenerateEditButton="true" OnModeChanging="ArticleModeChanging" DataKeyNames="ArticleKey" OnItemCommand="ArticleOnItemCommand" OnItemUpdating="ArticleOnItemUpdating" Width="100%">
            <Fields>
                <asp:BoundField HeaderText="Title" DataField="ArticleTitle" ControlStyle-Width="800" />
                <asp:BoundField HeaderText="Url" DataField="ArticleUrl" ControlStyle-Width="800" />
                <asp:CheckBoxField HeaderText="Is Review Article?" DataField="IsReview" ReadOnly="true" />
                <asp:BoundField HeaderText="Total Reviews" DataField="NumberOfReviews" ReadOnly="true" />
                <asp:BoundField HeaderText="Total Review Comments" DataField="NumberOfReviewComments" ReadOnly="true" />
                <asp:BoundField HeaderText="Total Article Comments" DataField="NumberOfComments" ReadOnly="true" />
                <asp:TemplateField HeaderText="Categories">
                    <ItemTemplate>
                        <%#Eval("Categories")%>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtCategories" runat="server" Width="800" Text='<%#Bind("Categories")%>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Section" DataField="Section" ControlStyle-Width="800" />
                <asp:BoundField HeaderText="SiteOfOrigin" DataField="SiteOfOrigin" ControlStyle-Width="800" />
                <asp:BoundField HeaderText="SourceSiteName" DataField="SourceSiteName" ControlStyle-Width="800" />
                <asp:BoundField HeaderText="SourceSiteKey" DataField="SourceSiteKey" ControlStyle-Width="800" />
            </Fields>
        </asp:DetailsView>

    </asp:PlaceHolder>

</asp:Content>
