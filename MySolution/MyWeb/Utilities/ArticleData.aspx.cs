﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PluckIL;

namespace MyWeb.Utilities
{
    public partial class ArticleData : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            if (!IsPostBack)
            {
                //Initially add the view state variable
                ViewState.Add("DetailsViewMode", DetailsViewMode.ReadOnly);
            }
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (ViewState["DetailsViewMode"] != null)
            {
                //change the mode of the DetailsView
                dvArticle.ChangeMode((DetailsViewMode)ViewState["DetailsViewMode"]);

                //Bind the details view to a source
                dvArticle.DataSource = Session["ArticleDataSource"];
                dvArticle.DataBind();
            }

            base.OnPreRender(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void GetArticle(object sender, EventArgs e)
        {
            string pluckStatus = String.Empty;
            plcResults.Visible = false;
            string articleKey = txtArticleKey.Text;
            if (!String.IsNullOrEmpty(articleKey))
            {
                Session["ArticleDataSource"] = ArticleHelpers.GetArticle(articleKey, out pluckStatus);
                if (Session["ArticleDataSource"] != null)
                {
                    dvArticle.DataSource = Session["ArticleDataSource"];
                    dvArticle.DataBind();
                    dvArticle.Visible = true;
                    plcPluckStatus.Visible = false;
                }
                else
                {
                    lblPluckStatus.Text = pluckStatus;
                    plcPluckStatus.Visible = true;
                    dvArticle.Visible = false;
                }
                plcData.Visible = true;
            }
        }

        protected void ArticleModeChanging(object sender, DetailsViewModeEventArgs e)
        {
        }

        //Handles button clicks from the DetailsView control
        protected void ArticleOnItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            ViewState["DetailsViewMode"] = GetDetailsViewMode(e.CommandName);
        }

        protected DetailsViewMode GetDetailsViewMode(string mode)
        {
            switch (mode)
            {
                case "Edit":
                    return DetailsViewMode.Edit;
                case "Insert":
                    return DetailsViewMode.Insert;
                default:
                    return DetailsViewMode.ReadOnly;
            }
        }

        protected void ArticleOnItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            Dictionary<string, string> articleData = new Dictionary<string, string>();
            articleData.Add("articleKey", e.Keys["ArticleKey"].ToString());
            articleData.Add("articleTitle", e.NewValues["ArticleTitle"].ToString());
            articleData.Add("articleUrl", e.NewValues["ArticleUrl"].ToString());
            articleData.Add("categories", e.NewValues["Categories"] != null ? e.NewValues["Categories"].ToString() : String.Empty);
            articleData.Add("section", e.NewValues["Section"] != null ? e.NewValues["Section"].ToString() : String.Empty);

            string responseStatus = String.Empty;
            ArticleHelpers.UpdateArticle(articleData, out responseStatus);
            GetArticle(null, null);
            plcResults.Visible = true;
            lblResult.Text = responseStatus;
        }
    }
}