﻿using PluckIL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.Utilities
{
    public partial class ContentPolicy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlActionType.DataSource = PolicyHelpers.GetActionTypes();
                ddlActionType.DataBind();
                ddlActionType.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                ddlDiscoveryActionType.DataSource = PolicyHelpers.GetActionTypes();
                ddlDiscoveryActionType.DataBind();
                ddlDiscoveryActionType.Items.Insert(0, new ListItem(String.Empty, String.Empty));

                ddlUserTier.DataSource = UserHelpers.GetUserTiers();
                ddlUserTier.DataBind();
                ddlUserTier.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                ddlDiscoveryUserTier.DataSource = UserHelpers.GetUserTiers();
                ddlDiscoveryUserTier.DataBind();
                ddlDiscoveryUserTier.Items.Insert(0, new ListItem(String.Empty, String.Empty));
            }
        }

        protected void GetContentPolicy(object sender, EventArgs e)
        {
            int totalItems = 0;
            string objectType = String.Empty;
            string pluckStatus = String.Empty;
            plcResults.Visible = false;
            string baseKey = txtBaseKey.Text;

            Session["Data"] = PolicyHelpers.GetContentPolicy(baseKey, ddlActionType.SelectedValue, ddlUserTier.SelectedValue, out totalItems, out objectType, out pluckStatus);
            if (Session["Data"] != null)
            {
                gvResults.DataSource = Session["Data"];
                gvResults.DataBind();
            }
            else
            {
                lblPluckStatus.Text = pluckStatus;
                plcPluckStatus.Visible = true;
            }
            plcResults.Visible = true;
        }

        protected void GetDiscoveryContentPolicy(object sender, EventArgs e)
        {
            int totalItems = 0;
            string objectType = String.Empty;
            string pluckStatus = String.Empty;
            plcResults.Visible = false;
            string category = txtCategory.Text;
            string section = txtSection.Text;

            Session["Data"] = PolicyHelpers.GetDiscoveryContentPolicy(category, section, ddlDiscoveryActionType.SelectedValue, ddlDiscoveryUserTier.SelectedValue, out totalItems, out objectType, out pluckStatus);
            if (Session["Data"] != null)
            {
                gvResults.DataSource = Session["Data"];
                gvResults.DataBind();
            }
            else
            {
                lblPluckStatus.Text = pluckStatus;
                plcPluckStatus.Visible = true;
            }
            plcResults.Visible = true;
        }
    }
}