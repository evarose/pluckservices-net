﻿<%@ Page Title="Pluck Switch" Language="C#" MasterPageFile="~/Masters/Utilities.master" AutoEventWireup="true" CodeBehind="PluckSwitch.aspx.cs" Inherits="MyWeb.Utilities.PluckSwitch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SDKToolContent" runat="server">

    <h4>Switching Pluck on/off</h4>

    <p>Sometimes, you may decide that you are not ready to enable Pluck yet, but the codes are ready OR you want to switch off Pluck temporarily for whatever reasons. Here, you can turn on and off Pluck by flicking the switch, or you can change the value of AppSettings["PluckEnabled"] directly in Web.Config file</p>

    <asp:RadioButtonList ID="rblSwitch" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SwitchPluck">
        <asp:ListItem Text="On" Value="On"></asp:ListItem>
        <asp:ListItem Text="Off" Value="Off"></asp:ListItem>
    </asp:RadioButtonList>

    <asp:PlaceHolder ID="plcPluckSwitch" runat="server" Visible="false">
        <pre class="blue"><asp:Label ID="lblPluckSwitch" runat="server"></asp:Label></pre>
    </asp:PlaceHolder>

    <hr style="border:0; border-bottom: 1px dotted #808080; width: 100%" />

    <h4>Checking Pluck availability</h4>
    <p>When Pluck is in the process of upgrading/patching, the platform is unavailable temporarily. Here, you can test Pluck availability. What the following function does is to change the value of AppSettings["PluckHost"] to a non-existent URL.</p>

    <asp:RadioButtonList ID="rblSwitchUrl" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SwitchPluckUrl">
        <asp:ListItem Text="Pluck reachable" Value="On"></asp:ListItem>
        <asp:ListItem Text="Pluck un-reachable" Value="Off"></asp:ListItem>
    </asp:RadioButtonList>

    <asp:PlaceHolder ID="plcPluckSwitchUrl" runat="server" Visible="false">
        <pre class="blue"><asp:Label ID="lblPluckSwitchUrl" runat="server"></asp:Label></pre>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="plcLogging" runat="server">
        <hr style="border: 0; border-bottom: 1px dotted #808080; width: 100%" />

        <h4>Switching PluckIL logging on/off</h4>
        <p>Here, you can switch on/off the logging feature of PluckIL helpers class. This DOES not effect your own application logging.</p>

        <asp:RadioButtonList ID="rblSwitchLogging" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SwitchPluckILLogging">
            <asp:ListItem Text="On" Value="On"></asp:ListItem>
            <asp:ListItem Text="Off" Value="Off"></asp:ListItem>
        </asp:RadioButtonList>

        <asp:PlaceHolder ID="plcSwitchLogging" runat="server" Visible="false">
            <pre class="blue"><asp:Label ID="lblSwitchLogging" runat="server"></asp:Label></pre>
        </asp:PlaceHolder>
    </asp:PlaceHolder>

</asp:Content>
