﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PluckIL;

namespace MyWeb.Utilities
{
    public partial class SearchContent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlContentType.DataSource = SearchHelpers.GetSearchTypes();
                ddlContentType.DataBind();
                ddlContentType.Items.Insert(0, new ListItem(String.Empty, String.Empty));
            }
        }

        protected void SearchData(object sender, EventArgs e)
        {
            int totalItems = 0;
            string objectType = String.Empty;
            string pluckStatus = String.Empty;
            plcResults.Visible = false;

            Session["Data"] = SearchHelpers.SearchPluck(ddlContentType.SelectedValue, txtKeyword.Text, out totalItems, out objectType, out pluckStatus);
            if (Session["Data"] != null)
            {
                dvData.DataSource = Session["Data"];
                dvData.DataBind();

                lblTotalItems.Text = totalItems.ToString();
                lblObjectType.Text = objectType;
                plcData.Visible = true;
                plcPluckStatus.Visible = false;
            }
            else
            {
                lblPluckStatus.Text = pluckStatus;
                plcData.Visible = false;
                plcPluckStatus.Visible = true;
            }
            plcResults.Visible = true;
        }

        protected void PageIndexChanging(object sender, DetailsViewPageEventArgs e)
        {
            dvData.PageIndex = e.NewPageIndex;
            dvData.DataSource = Session["Data"];
            dvData.DataBind();
        }
    }
}