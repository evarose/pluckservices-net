﻿using PluckIL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.Utilities
{
    public partial class Rewards : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AwardPoints(object sender, EventArgs e)
        {
            string pluckStatus = String.Empty;
            string userKey = txtUserKey.Text;
            string currencyType = txtCurrency.Text;
            int pointsToAward = Convert.ToInt32(String.IsNullOrEmpty(txtPoints.Text) ? "0" : txtPoints.Text);
            if (!String.IsNullOrEmpty(userKey) && !String.IsNullOrEmpty(currencyType) && pointsToAward != 0)
            {
                RewardHelpers.AwardPointsToUser(userKey, currencyType, pointsToAward, out pluckStatus);
                plcPluckStatus.Visible = true;
                lblPluckStatus.Text = pluckStatus;
                gvData.Visible = false;
            }
        }

        protected void GetPoints(object sender, EventArgs e)
        {
            string pluckStatus = String.Empty;
            string userKey = txtUserKey.Text;
            string currencyType = txtCurrency.Text;
            if (!String.IsNullOrEmpty(userKey) && !String.IsNullOrEmpty(currencyType))
            {
                lblPluckStatus.Text = "User points for currency '" + currencyType + "': " + RewardHelpers.GetPointsForUser(userKey, currencyType, out pluckStatus).ToString();
                plcPluckStatus.Visible = true;
                gvData.Visible = false;
            }
        }

        protected void GetActivity(object sender, EventArgs e)
        {
            string pluckStatus = String.Empty;
            string userKey = txtUserKey.Text;
            if (!String.IsNullOrEmpty(userKey))
            {
                Array activities = RewardHelpers.GetPointsAndBadgingActivity(userKey, out pluckStatus);
                gvData.DataSource = activities;
                gvData.DataBind();
                plcPluckStatus.Visible = true;
                lblPluckStatus.Text = pluckStatus;
                gvData.Visible = true;
            }
        }

        protected void SendBadgingEvent(object sender, EventArgs e)
        {
            string pluckStatus = String.Empty;
            string activityName = txtCustomActivity.Text;
            string[] activityTags = txtActivityTags.Text.Split(',');
            string[] userKeys = txtUserKeys.Text.Split(',');
            if (!String.IsNullOrEmpty(activityName) && userKeys.Length != 0)
            {
                RewardHelpers.SendBadgingEvent(activityName, activityTags, userKeys, out pluckStatus);
                plcPluckStatus.Visible = true;
                lblPluckStatus.Text = pluckStatus;
                gvData.Visible = false;
            }
        }
    }
}