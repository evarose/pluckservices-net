﻿<%@ Page Title="Search Content" Language="C#" MasterPageFile="~/Masters/Utilities.master" AutoEventWireup="true" CodeBehind="SearchContent.aspx.cs" Inherits="MyWeb.Utilities.SearchContent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SDKToolContent" runat="server">

    <h4>Search Pluck Content</h4>

    <p>The search functionality uses PluckIL.SearchHelpers (which calls up Pluck SDK SearchActionRequest)</p>

    <table border="0" class="sdk-table">
        <tr>
            <td>Search type:</td>
            <td>
                <asp:DropDownList ID="ddlContentType" runat="server" AutoPostBack="false" /></td>
        </tr>
        <tr>
            <td>Keyword:</td>
            <td>
                <asp:TextBox ID="txtKeyword" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="SearchData" CssClass="btn btn-primary btn-large" /></td>
        </tr>
    </table>

    <asp:PlaceHolder ID="plcResults" runat="server" Visible="false">

        <hr style="border: 0; border-bottom: 1px dotted #808080; width: 100%" />

        <asp:PlaceHolder ID="plcPluckStatus" runat="server" Visible="false">
            <pre class="red"><asp:Label ID="lblPluckStatus" runat="server"></asp:Label></pre>
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="plcData" runat="server" Visible="false">
            <table border="1" cellpadding="4" cellspacing="0" style="border-collapse: collapse">
                <tr>
                    <td>Total items:</td>
                    <td>
                        <asp:Label ID="lblTotalItems" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>Items of object type:</td>
                    <td>
                        <asp:Label ID="lblObjectType" runat="server"></asp:Label></td>
                </tr>
            </table>

            <br />

            <asp:DetailsView ID="dvData" runat="server" AutoGenerateRows="true" CellPadding="4" AllowPaging="true" OnPageIndexChanging="PageIndexChanging" PagerSettings-Position="TopAndBottom" EnableViewState="true">
            </asp:DetailsView>
        </asp:PlaceHolder>

    </asp:PlaceHolder>

</asp:Content>
