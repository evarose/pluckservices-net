﻿<%@ Page Title="Content Policy" Language="C#" MasterPageFile="~/Masters/Utilities.master" AutoEventWireup="true" CodeBehind="ContentPolicy.aspx.cs" Inherits="MyWeb.Utilities.ContentPolicy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SDKToolContent" runat="server">

    <h4>Content Policy for Single object</h4>

    <table border="0" class="sdk-table">
        <tr>
            <td>Base key:</td>
            <td>
                <asp:TextBox ID="txtBaseKey" runat="server" Text="myTestArticle.html"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Action type:</td>
            <td>
                <asp:DropDownList ID="ddlActionType" runat="server" AutoPostBack="false" /></td>
        </tr>
        <tr>
            <td>User tier:</td>
            <td>
                <asp:DropDownList ID="ddlUserTier" runat="server" AutoPostBack="false" /></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnGetContentPolicy" runat="server" Text="Get Content Policy" OnClick="GetContentPolicy" CssClass="btn btn-primary btn-large" /></td>
        </tr>
    </table>

    <h4>Content Policy for Category/Section</h4>

    <table border="0" class="sdk-table">
        <tr>
            <td>Category:</td>
            <td>
                <asp:TextBox ID="txtCategory" runat="server" Text=""></asp:TextBox></td>
        </tr>
        <tr>
            <td>Section:</td>
            <td>
                <asp:TextBox ID="txtSection" runat="server" Text=""></asp:TextBox></td>
        </tr>
        <tr>
            <td>Action type:</td>
            <td>
                <asp:DropDownList ID="ddlDiscoveryActionType" runat="server" AutoPostBack="false" /></td>
        </tr>
        <tr>
            <td>User tier:</td>
            <td>
                <asp:DropDownList ID="ddlDiscoveryUserTier" runat="server" AutoPostBack="false" /></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnGetDiscoveryContentPolicy" runat="server" Text="Get Discovery Content Policy" OnClick="GetDiscoveryContentPolicy" CssClass="btn btn-primary btn-large" /></td>
        </tr>
    </table>

    <asp:PlaceHolder ID="plcResults" runat="server" Visible="false">

        <hr style="border: 0; border-bottom: 1px dotted #808080; width: 100%" />

        <asp:PlaceHolder ID="plcPluckStatus" runat="server" Visible="false">
            <pre class="red"><asp:Label ID="lblPluckStatus" runat="server"></asp:Label></pre>
        </asp:PlaceHolder>

        <asp:GridView ID="gvResults" runat="server" AutoGenerateColumns="true"></asp:GridView>

    </asp:PlaceHolder>

</asp:Content>
