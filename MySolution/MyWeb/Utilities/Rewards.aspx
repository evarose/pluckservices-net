﻿<%@ Page Title="Rewards Utility" Language="C#" MasterPageFile="~/Masters/Utilities.master" AutoEventWireup="true" CodeBehind="Rewards.aspx.cs" Inherits="MyWeb.Utilities.Rewards" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SDKToolContent" runat="server">

    <h4>Award Points/Get Points/Get Points & Badging Activity</h4>

    <table border="0" class="sdk-table">
        <tr>
            <td>User key:</td>
            <td>
                <asp:TextBox ID="txtUserKey" runat="server" Text=""></asp:TextBox></td>
        </tr>
        <tr>
            <td>Leaderboard currency:</td>
            <td>
                <asp:TextBox ID="txtCurrency" runat="server" Text=""></asp:TextBox></td>
        </tr>
        <tr>
            <td>Points to award:</td>
            <td>
                <asp:TextBox ID="txtPoints" runat="server" Text=""></asp:TextBox></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnAwardPoints" runat="server" Text="Award Points" OnClick="AwardPoints" CssClass="btn btn-primary btn-large" />
                <asp:Button ID="btnGetPoints" runat="server" Text="Get Points" OnClick="GetPoints" CssClass="btn btn-primary btn-large" />
                <asp:Button ID="btnGetActivity" runat="server" Text="Get Points & Badging Activity" OnClick="GetActivity" CssClass="btn btn-primary btn-large" />
            </td>
        </tr>
    </table>

    <h4>Send Badging Event</h4>

    <table border="0" class="sdk-table">
        <tr>
            <td>Custom activity name*:</td>
            <td>
                <asp:TextBox ID="txtCustomActivity" runat="server" Text=""></asp:TextBox></td>
        </tr>
        <tr>
            <td>User keys (separate with comma)*:</td>
            <td>
                <asp:TextBox ID="txtUserKeys" runat="server" Text=""></asp:TextBox></td>
        </tr>
        <tr>
            <td>Custom activity tags (separate with comma):</td>
            <td>
                <asp:TextBox ID="txtActivityTags" runat="server" Text=""></asp:TextBox></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnSendBadgingEvent" runat="server" Text="Submit Badging Event" OnClick="SendBadgingEvent" CssClass="btn btn-primary btn-large" /></td>
        </tr>
    </table>

    <asp:PlaceHolder ID="plcResults" runat="server" Visible="true">

        <hr style="border: 0; border-bottom: 1px dotted #808080; width: 100%" />

        <asp:PlaceHolder ID="plcPluckStatus" runat="server" Visible="false">
            <pre class="red"><asp:Label ID="lblPluckStatus" runat="server"></asp:Label></pre>
        </asp:PlaceHolder>

        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="true" CellPadding="4"></asp:GridView>
    </asp:PlaceHolder>

</asp:Content>
