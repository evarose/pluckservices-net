﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PluckIL;

namespace MyWeb.Utilities
{
    public partial class PluckSwitch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                CheckPluckStatus();
        }

        protected void CheckPluckStatus()
        {
            if (!PluckConfig.IsPluckConfigured())
                return;

            switch (PluckConfig.IsPluckEnabled())
            {
                case true:
                    rblSwitch.Items[0].Selected = true;
                    break;
                case false:
                    rblSwitch.Items[1].Selected = true;
                    break;
            }

            switch (PluckConfig.IsPluckAvailable())
            {
                case true:
                    rblSwitchUrl.Items[0].Selected = true;
                    break;
                case false:
                    rblSwitchUrl.Items[1].Selected = true;
                    break;
            }

            plcLogging.Visible = PluckConfig.IsLoggingConfigured();

            switch (PluckConfig.IsLoggingEnabled())
            {
                case true:
                    rblSwitchLogging.Items[0].Selected = true;
                    break;
                case false:
                    rblSwitchLogging.Items[1].Selected = true;
                    break;
            }
        }

        protected void SwitchPluck(object sender, EventArgs e)
        {
            plcPluckSwitch.Visible = true;
            plcPluckSwitchUrl.Visible = false;
            plcSwitchLogging.Visible = false;
            switch (rblSwitch.SelectedValue)
            {
                case "On":
                    PluckConfig.EnablePluck();
                    lblPluckSwitch.Text = "Pluck is now enabled.";
                    break;
                case "Off":
                    PluckConfig.DisablePluck();
                    lblPluckSwitch.Text = "Pluck is now disabled.";
                    break;
            }
        }

        protected void SwitchPluckUrl(object sender, EventArgs e)
        {
            plcPluckSwitch.Visible = false;
            plcPluckSwitchUrl.Visible = true;
            plcSwitchLogging.Visible = false;
            switch (rblSwitchUrl.SelectedValue)
            {
                case "On":
                    PluckConfig.SwitchPluckUrl("http://presales.pluck.com");
                    lblPluckSwitchUrl.Text = "Pluck is now reachable.";
                    break;
                case "Off":
                    PluckConfig.SwitchPluckUrl("http://nonexistent.pluck.com");
                    lblPluckSwitchUrl.Text = "Pluck is now un-reachable.";
                    break;
            }
        }

        protected void SwitchPluckILLogging(object sender, EventArgs e)
        {
            plcPluckSwitch.Visible = false;
            plcPluckSwitchUrl.Visible = false;
            plcSwitchLogging.Visible = true;
            switch (rblSwitchLogging.SelectedValue)
            {
                case "On":
                    PluckConfig.SwitchLogging(true);
                    lblSwitchLogging.Text = "PluckIL logging is now on";
                    break;
                case "Off":
                    PluckConfig.SwitchLogging(false);
                    lblSwitchLogging.Text = "PluckIL logging is now off";
                    break;
            }
            
        }
    }
}