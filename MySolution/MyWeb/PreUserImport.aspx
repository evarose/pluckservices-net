﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreUserImport.aspx.cs" Inherits="MyWeb.PreUserImport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="panPostData" runat="server" Visible="false">
                <p>
                    Parameters (Form Data):
                <br />
                    <asp:Label ID="lblPostData" runat="server"></asp:Label>
                    <br />
                    Text written to:
                    <asp:Label ID="lblPostFile" runat="server"></asp:Label>
                </p>
            </asp:Panel>

            <asp:Panel ID="panGetData" runat="server" Visible="false">
                <p>
                    Parameters (QueryString Data):
                <br />
                    <asp:Label ID="lblGetData" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblTestData" runat="server" />
                    <br />
                    Text written to:
                    <asp:Label ID="lblGetFile" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
