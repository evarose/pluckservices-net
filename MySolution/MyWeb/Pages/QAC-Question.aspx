﻿<%@ Page Title="Question" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="QAC-Question.aspx.cs" Inherits="MyWeb.Pages.QAC_Question" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Q&A using Multi-Comments.</h2>

    <a href="javascript:history.back()"><< Back to Landing Page</a>

    <script type="text/javascript">
        pluckAppProxy.registerActivityCallback("CommentsRendered", addTags());
        var commentTag = '<div class="comment-tags">Tags: <input id="tags" type="text" /></div>';
        var tags = {};
        function addTags() {

            $('#question').ready(function () {
                console.log(commentTag);
                $(commentTag).insertAfter($('.pluck-comm-action-controls'));
            });

            $('#answer').ready(function () {
                console.log('answer ready');
            });
            //$('<div class="comment-tags">Tags: <input id="tags" type="text" /></div>').insertAfter($('.pluck-comm-action-controls'));
            // Insert new div to allow tags in each comment

            //$('.pluck-comm-single-comment-main').each(function () {
            //    var commentKey = $(this).attr('commentid');
            //    confirm(commentKey);
            //    var commentActions = $(commentKey).child('.pluck-comm-comment-content').child('.pluck-comm-action-controls');
            //    $('<div class="comment-tags">Tags: <input id="tags" type="text" /></div>').insertAfter($(commentActions));
            //});
        }
    </script>

    <h3><%: Title %>: <asp:Label ID="lblQuestionTitle" runat="server" CssClass="question-title"></asp:Label></h3>

    <div id="question"></div>
    <script type="text/javascript">
        var opts = { elem: "question" }
        pluckAppProxy.embedApp("pluck_comments", {
            plckCommentOnKeyType: "article",
            plckCommentOnKey: "<%=QuestionKey%>",
            plckCommentSubmitDisabled: "true",
            plckCommentListType: "narrow",
            pckppp: "http://<%=Request.Url.DnsSafeHost%>/Pages/Persona"
        }, opts);
    </script>


    <h3>Expert Answer</h3>

    <div id="answer"></div>
    <script type="text/javascript">
        var opts = { elem: "answer" }
        pluckAppProxy.embedApp("pluck_comments", {
            plckCommentOnKeyType: "article",
            plckCommentOnKey: "<%=AnswerKey%>",
            plckCommentSubmitDisabled: "<%=CommentDisabled%>",
            plckCommentListType: "narrow",
            pckppp: "http://<%=Request.Url.DnsSafeHost%>/Pages/Persona"
        }, opts);
    </script>

    <h3>Replies</h3>

    <script type="text/javascript">
        var opts = {}
        pluckAppProxy.embedApp("pluck_comments", {
            plckCommentOnKeyType: "article",
            plckCommentOnKey: "<%=RepliesKey%>",
            plckCommentListType: "narrow",
            pckppp: "http://<%=Request.Url.DnsSafeHost%>/Pages/Persona"
        }, opts);
    </script>

</asp:Content>
