﻿<%@ Page Title="Q&A using Forums" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="QAF.aspx.cs" Inherits="MyWeb.Pages.QAForums" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <script type="text/javascript">
        pluckAppProxy.registerActivityCallback("ForumsDiscussionCreateRendered", amendDiscussionCreateForm);

        function amendDiscussionCreateForm() {
            // Hide "Add a poll" option
            $('.pluck-forums-discussionCreate-option:first-child').hide();

            // Automatically select this discussion as a question
            $('.pluck-forums-discussionCreate-isQuestion').prop('checked', true);
        }
    </script>

    <PluckIL:Rest runat="server" AppName="Forums" ShowRestUrl="false">
        <PluckIL:WidgetParameter Key="plckForumPage" Value="ForumCategory" />
        <PluckIL:WidgetParameter Key="plckCategoryId" Value="Cat:93ad4f25-8a2e-44c7-b2e7-4cfea9e0e0e0" />
        <PluckIL:WidgetParameter Key="pckfpp" Value="http://sandbox.pluck.com/Pages/QAF/" />
    </PluckIL:Rest>

</asp:Content>
