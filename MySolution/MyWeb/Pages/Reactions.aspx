﻿<%@ Page Title="Reactions" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Reactions.aspx.cs" Inherits="MyWeb.Reactions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <p>This is Pluck User Reactions using REST.</p>

    <PluckIL:Rest runat="server" AppName="Reactions" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckUserReactionOnKeyType" Value="article" />
        <PluckIL:WidgetParameter Key="plckUserReactionOnKey" Value="myTestUserReaction.html" />
        <PluckIL:WidgetParameter Key="plckUserReactionChoiceKeys" Value="want-it-now, not-my-style, get-out-of-my-face" />
    </PluckIL:Rest>

</asp:Content>
