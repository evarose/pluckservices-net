﻿<%@ Page Title="Forums" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Forums.aspx.cs" Inherits="MyWeb.Forums" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <p>This is Pluck Forums using REST.</p>

    <PluckIL:Rest runat="server" AppName="Forums" ShowRestUrl="true"></PluckIL:Rest>

</asp:Content>
