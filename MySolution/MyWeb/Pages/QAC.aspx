﻿<%@ Page Title="Q&A using Multi-Comments" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="QAC.aspx.cs" Inherits="MyWeb.Pages.QAComments" %>
<asp:Content ID="Header1" ContentPlaceHolderID="HeaderContent" runat="server">
     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script type="text/javascript">
        var j = jQuery.noConflict();
    </script>
    <style type="text/css">
        .ui-autocomplete-loading {
            background: white url("/Content/Images/ui-anim_basic_16x16.gif") left center no-repeat !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Q&A using Multi-Comments (Ask The Expert)</h2>

    <p>"Ask The Expert" works on the basis that only 1 answer (from an expert) is allowed to 1 question, and many possible answers (from which users can score) come from community users. This means, possible answers cannot be an expert answer or chosen as a definite answer. On this note, the best solution for "Ask The Expert" system is to make use of 3 comment widgets.</p>

    <h3>Search Q&A:</h3>

    <script type="text/javascript">
        $(function () {
            j($('#search-box')).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "QA-Search.aspx",
                        dataType: "json",
                        data: {
                            q: request.term
                        },
                        success: function (data) {
                            response(
                                $.map(data, function (obj) {
                                    return {
                                        label: obj.ArticleKey,
                                        value: obj.CommentBody,
                                        comment: obj.CommentBody,
                                        avatar: obj.AuthorAvatarUrl
                                    };
                                })
                            );
                        }
                    })
                    //.done(function () {
                    //    confirm("success");
                    //});
                },
                minLength: 3
            })
            .autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a href='/Pages/QAC-Question?ID=" + item.label + "' style='float: left; display: inline-block;'><img class='user-avatar' src='" + item.avatar + "' style='padding-right: 10px; float: left;' />" + item.comment + "</a>")
                .appendTo(ul);
            };
        });
    </script>

    <input type="text" id="search-box" />

    <p style="padding-top: 8px;">Can't find your answer? <a href="/Pages/QAC-Ask">Ask!</a></p>

    <div class="categories">
        Categories: <asp:Label ID="lblTags" runat="server"></asp:Label>
    </div>

    <div style="padding-top: 8px">

        <h3>Recent questions & answers <asp:Label ID="lblCategory" runat="server"></asp:Label></h3>

        <asp:Repeater ID="rptData" runat="server" OnItemDataBound="rptData_OnItemDataBound">
            <HeaderTemplate>
                <table cellspacing="0" rules="all" border="0" style="border-collapse: collapse; width: 100%">
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="qa-item">
                    <td style="padding: 10px; border: 1px dotted #808080">
                        <asp:HiddenField ID="hidQuestionKey" Value='<%#Eval("QuestionKey")%>' runat="server" />
                        <div class="question-title">
                            <a href='/Pages/QAC-Question?ID=<%#Eval("QuestionKey")%>'><%#Eval("QuestionTitle") %></a>
                        </div>
                        <div class="question-avatar">
                            <asp:Image ID="imgQuestioner" runat="server" AlternateText='<%#Eval("QuestionerName")%>' BorderWidth="0" ImageUrl='<%#Eval("QuestionerAvatarUrl")%>' CssClass="user-avatar" />
                            <br />
                            <asp:HyperLink ID="lnkQuestioner" runat="server" Text='<%#Eval("QuestionerName")%>' NavigateUrl='<%# String.Format("/Pages/Persona?UID={0}", Eval("QuestionerKey")) %>'></asp:HyperLink>
                            <br />
                            <%#Eval("QuestionDatePosted")%>
                        </div>
                        <div class="bubble">
                            <asp:Label ID="lblQuestion" runat="server" Text='<%#Eval("QuestionBody")%>'></asp:Label>
                        </div>

                        <asp:HiddenField ID="hidAnswerKey" Value='<%#Eval("AnswerKey")%>' runat="server" />
                        <div class="bubble-answer">
                            <asp:Label ID="lblExpertAnswer" runat="server" Text='<%#Eval("AnswerBody")%>'></asp:Label>
                        </div>
                        <div class="answer-avatar">
                            <asp:Image ID="imgAnswerer" runat="server" AlternateText='<%#Eval("AnswererName")%>' BorderWidth="0" ImageUrl='<%#Eval("AnswererAvatarUrl")%>' CssClass="user-avatar" />
                            <br />
                            <asp:HyperLink ID="lnkAnswerer" runat="server" Text='<%#Eval("AnswererName")%>' NavigateUrl='<%# String.Format("/Pages/Persona?UID={0}", Eval("AnswererKey")) %>'></asp:HyperLink>
                            <br />
                            <%#Eval("AnswerDatePosted").ToString().Contains("0001") ? String.Empty : Eval("AnswerDatePosted")%>
                        </div>

                        <div class="info-box">
                            <%#Eval("NumberOfReplies") %> replies<br />
                            Category: <%# Eval("QuestionCategories") != null ? DisplayCategories(Eval("QuestionCategories").ToString()) : String.Empty%><br />
                            <a href='/Pages/QAC-Question?ID=<%#Eval("QuestionKey")%>'>See thread</a>
                        </div>

                        <div class="tags">
                            <ul class="tags-list">
                                <li class="tag"><a href="#">one</a></li>
                                <li class="tag"><a href="#">two</a></li>
                                <li class="tag"><a href="#">three</a></li>
                            </ul>
                        </div>

                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    
</asp:Content>
