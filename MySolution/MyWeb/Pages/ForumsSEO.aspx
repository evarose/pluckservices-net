﻿<%@ Page Title="SEO Forums" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="ForumsSEO.aspx.cs" Inherits="MyWeb.Pages.ForumsSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <p>This is Pluck Forums using REST. For SEO forums to work within PluckIL control, you'd pass the following:</p>
    <ul>
        <li>SEOEnabled = true</li>
        <li>SEORootUrl = the root of your forum where SEO url will start from</li>
        <li>plckRefreshPage = true - this means that every click in the widget will reload the entire page (as opposed to via AJAX), from which the SEO url is returned. It is THIS SEO url that PluckIL does all the magic</li>
        <li>pckfpp - this is PCK override is used within the widget to ensure you're pointing to the correct location of your SEO forum. Essentially, this value is the same as SEORootUrl</li>
    </ul>

    <script type="text/javascript">
        pluckAppProxy.registerActivityCallback("ForumsDiscussionListRendered", removeDiscussionLayOver);

        function removeDiscussionLayOver() {
            $('.pluck-forums-discussionList').find('.pluck-forums-main-discussion-icon-col').each(function () {
                $(this).attr('discussionkey', '');
            });
            $('.pluck-forums-discussionList').find('.pluck-forums-main-discussion-name-col .pluck-forums-discussion-title-link').each(function () {
                $(this).attr('discussionkey', '');
            });
        };
    </script>

    <%--
    To use this control with SEO, all you have to do is to pass SEOEnabled=true, set the SEORootUrl for forums and pass the widget parameters as the following:
     --%>
    <PluckIL:Rest runat="server" AppName="Forums" ShowRestUrl="true" SEOEnabled="true" SEORootUrl="http://sandbox.pluck.com/Pages/ForumsSEO/">
        <PluckIL:WidgetParameter Key="plckRefreshPage" Value="true" />
        <PluckIL:WidgetParameter Key="pckfpp" Value="http://sandbox.pluck.com/Pages/ForumsSEO/" />
        <PluckIL:WidgetParameter Key="pckcbu" Value="http://sandbox.pluck.com" />
        <PluckIL:WidgetParameter Key="pckppp" Value="http://sandbox.pluck.com/Pages/Persona/" />
    </PluckIL:Rest>

</asp:Content>
