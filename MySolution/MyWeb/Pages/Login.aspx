﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MyWeb.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <asp:PlaceHolder ID="plcSSOForm" runat="server" Visible="false">
        <h3>Login with Pluck Single Sign On</h3>

        <p>You'll provide a login form here, with username & password, from which your system will validate against the user.</p>

        <p>Once validated, then you will be logged into Pluck by creating "at" cookie. Assuming that you have validated the user against your own registration system, the form below takes the User ID, Display Name & Email of the validated user and creates SSO cookie.</p>

        <asp:PlaceHolder ID="plcLoginForm" runat="server">
            <table class="sdk-table">
                <tbody>
                    <tr>
                        <td>User ID:</td>
                        <td>
                            <asp:TextBox ID="txtUserKey" runat="server" Width="600"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Display Name:</td>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" Width="600"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" Width="600"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Set as HttpOnly cookie</td>
                        <td>
                            <asp:RadioButtonList ID="rblHttpOnly" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="False" Selected="True"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <PluckIL:SSO ID="pilSSO" runat="server" CssClass="btn btn-primary btn-large" UserKey="" DisplayName="" Email="" HttpOnly="false" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:PlaceHolder>

        <h3>Login with Pluck Social Sign On</h3>

        <p>This Social Sign On is using JavaScript widget (NOT server-side). This functionality demonstrates logging onto Pluck via Facebook, which will bring the user email from Facebook into Pluck AT cookie (only applies to new FB users who have never used this login before). Social Sign On requires you to have Facebook App Id & Secret Key defined in configuration first.</p>

        <script type="text/javascript">
            var opts = {}
            pluckAppProxy.embedApp("pluck_socialSignOn", {
                plckProviders: "facebook,googleplus",
                plckLogoutUrl: "/Security/Logout"
            }, opts);
        </script>

    </asp:PlaceHolder>

    <asp:PlaceHolder ID="plcUserLogged" runat="server" Visible="false">
        <p>
            You are logged in as
            <asp:Label ID="lblUser" runat="server" Font-Bold="true"></asp:Label>
        </p>

        <p>
            Check
            <asp:HyperLink ID="lnkPluckHost" runat="server" Target="_blank"></asp:HyperLink>
            and see whether you are automatically logged in via Otterfest.
        </p>
    </asp:PlaceHolder>

</asp:Content>
