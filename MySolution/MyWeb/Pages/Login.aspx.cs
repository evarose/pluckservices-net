﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using PluckIL;

namespace MyWeb
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckLoggedUser();
            if (IsPostBack)
            {
                // Pluck authentication happens here, check PluckIL.SSO event handler of "OnClick_Login"
                pilSSO.UserKey = txtUserKey.Text;
                pilSSO.DisplayName = txtUserName.Text;
                pilSSO.Email = txtEmail.Text;
                pilSSO.HttpOnly = Boolean.Parse(rblHttpOnly.SelectedValue);
            }
        }

        protected void CheckLoggedUser()
        {
            plcSSOForm.Visible = true;
            plcUserLogged.Visible = false;
            string userKey = SSO.GetATCookieValue("u");
            if (!String.IsNullOrEmpty(userKey))
            {
                lnkPluckHost.NavigateUrl = WebConfigurationManager.AppSettings["PluckHost"];
                lnkPluckHost.Text = WebConfigurationManager.AppSettings["PluckHost"];
                lblUser.Text = SSO.GetATCookieValue("a");
                plcUserLogged.Visible = true;
                plcSSOForm.Visible = false;
            }
        }
    }
}