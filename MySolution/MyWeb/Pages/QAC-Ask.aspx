﻿<%@ Page Title="Ask A Question" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="QAC-Ask.aspx.cs" Inherits="MyWeb.Pages.QAComments_Ask" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Q&A using Multi-Comments.</h2>

    <h3><%: Title %></h3>

    <ul>
        <li>This page uses SDK to create <u>3 ARTICLES to represent 3 SEPARATE COMMENT WIDGETS that make up 1 Q&A ITEM</u></li>
        <li>Each article being created needs 1 BASE ARTICLE KEY (say "ARTICLE1"), and upon creation, the articles will be appended with a character, as follows:
            <ul>
                <li>ARTICLE1-Q : To represent a question</li>
                <li>ARTICLE1-E : To represent an expert answer</li>
                <li>ARTICLE1-R : To represent replies/possible answers</li>
            </ul>
        </li>
    </ul>

    <h3>Question:</h3>

    <table class="question-table">
        <tr>
            <td>Base article key:</td>
            <td><asp:TextBox ID="txtBaseArticleKey" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Question title:</td>
            <td><asp:TextBox ID="txtQuestionTitle" runat="server" Width="800"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Question body:</td>
            <td><asp:TextBox ID="txtQuestionBody" runat="server" TextMode="MultiLine" Rows="2" CssClass="question-box"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Category: </td>
            <td>
                <asp:DropDownList ID="ddlCategory" runat="server">
                    <asp:ListItem Text="General" Value="General"></asp:ListItem>
                    <asp:ListItem Text="Coffee Machines" Value="Coffee-Machines"></asp:ListItem>
                    <asp:ListItem Text="Entertainment" Value="Entertainment"></asp:ListItem>
                    <asp:ListItem Text="Music" Value="Music"></asp:ListItem>
                    <asp:ListItem Text="Travel" Value="Travel"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Section: </td>
            <td>
                <asp:TextBox ID="txtSection" runat="server" Text="Sandbox-Ask-The-Expert" Width="100%"></asp:TextBox>
                <br />
                <i>Assigning this to a section would assist in discovery of all articles created in this section only , so that it separates from all other articles that use comment widgets</i>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Submit question" OnClick="SubmitQuestion" CssClass="btn btn-primary btn-large" />
            </td>
        </tr>
    </table>

    <asp:PlaceHolder ID="plcMessage" runat="server" Visible="false">
        <pre class="red" style="margin-top: 10px;"><asp:Label ID="lblMessage" runat="server"></asp:Label></pre>
    </asp:PlaceHolder>
    
</asp:Content>
