﻿using PluckIL;
using PluckIL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.Pages
{
    public partial class QAC_Question : System.Web.UI.Page
    {
        protected string QuestionKey = String.Empty;
        protected string AnswerKey = String.Empty;
        protected string RepliesKey = String.Empty;
        protected string CommentDisabled = "false";

        protected void Page_Load(object sender, EventArgs e)
        {
            string articleKey = Request.QueryString["ID"];
            string baseArticleKey = articleKey;
            QuestionKey = articleKey;

            if (articleKey.Contains("-"))
            {
                baseArticleKey = articleKey.Substring(0, articleKey.IndexOf("-"));
            }

            QuestionKey = baseArticleKey + "-Q";
            string pluckStatus = String.Empty;
            lblQuestionTitle.Text = ArticleHelpers.GetArticle(QuestionKey, out pluckStatus).Cast<ViewModels.ArticleModel>().Select(a => a.ArticleTitle).SingleOrDefault();
            AnswerKey = baseArticleKey + "-E";
            RepliesKey = baseArticleKey + "-R";

            string currentUserTier = String.Empty;
            int totalComments = 0;

            QAHelpers.CheckExpertAnswer(AnswerKey, SSO.GetATCookieValue("u"), out totalComments, out currentUserTier);

            // if number of top level comments = 1, then we have an answer, so disable comments
            if (totalComments > 0)
            {
                CommentDisabled = "true";
            }
            else
            {
                if (currentUserTier != "Featured")
                {
                    CommentDisabled = "true";
                }
            }
        }
    }
}