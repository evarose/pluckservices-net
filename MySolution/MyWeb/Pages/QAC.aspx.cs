﻿using PluckIL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.Pages
{
    public partial class QAComments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string category = Request.QueryString["cat"];
            if (!String.IsNullOrEmpty(category))
            {
                string categoryName = QAHelpers.ListCategories().Where(c => c.Key == category).Select(c => c.Value).SingleOrDefault().ToString();
                lblCategory.Text = " in \"" + categoryName + "\" (<a href='/Pages/QAC'>Show all</a>)";
            }
            GetTags();
            GetRecentQuestions(category);
        }

        protected void GetTags()
        {
            Dictionary<string, string> categories = QAHelpers.ListCategories();
            lblTags.Text = string.Join(", ", categories.Select(c => "<a href='?cat=" + c.Key + "'>" + c.Value + "</a>").ToArray());
        }

        protected void GetRecentQuestions(string category)
        {
            List<string> categories = new List<string> { "question" };
            if (!String.IsNullOrEmpty(category))
            {
                categories.Add(category);
            }
            rptData.DataSource = QAHelpers.GetRecentQA("Sandbox-Ask-The-Expert", categories, 5);
            rptData.DataBind();
        }

        protected void rptData_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hidQuestionKey = (HiddenField)e.Item.FindControl("hidQuestionKey");
                string questionKey = hidQuestionKey.Value;
                Label lblQuestion = (Label)e.Item.FindControl("lblQuestion");
                if (lblQuestion.Text.Length > 125)
                {
                    lblQuestion.Text = lblQuestion.Text.Substring(0, 125) + " ... <a href='/Pages/QAC-Question?ID=" + questionKey + "'>more</a>";
                }

                HiddenField hidAnswerKey = (HiddenField)e.Item.FindControl("hidAnswerKey");
                string answerKey = hidAnswerKey.Value;
                Label lblExpertAnswer = (Label)e.Item.FindControl("lblExpertAnswer");
                if (lblExpertAnswer.Text.Length > 125)
                {
                    lblExpertAnswer.Text = lblExpertAnswer.Text.Substring(0, 125) + " ... <a href='/Pages/QAC-Question?ID=" + answerKey + "'>more</a>";
                }
            }
        }

        protected string DisplayCategories(string categories)
        {
            StringBuilder sb = new StringBuilder();
            string[] categoryList = categories.Split(',');
            for (int i = 0; i < categoryList.Count(); i++)
            {
                if (i == categoryList.Count() - 1)
                    sb.Append("<a href='?cat=" + categoryList[i] + "'>" + categoryList[i] + "</a>");
                else
                    sb.Append("<a href='?cat=" + categoryList[i] + "'>" + categoryList[i] + "</a>, ");
            }
            return sb.ToString();
        }
    }
}