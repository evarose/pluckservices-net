﻿<%@ Page Title="Persona" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Persona.aspx.cs" Inherits="MyWeb.Persona" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <p>This is Pluck Persona using REST.</p>

    <PluckIL:Rest runat="server" AppName="Persona" ShowRestUrl="true" />

</asp:Content>
