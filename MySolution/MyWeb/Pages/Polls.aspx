﻿<%@ Page Title="Polls" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Polls.aspx.cs" Inherits="MyWeb.Polls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <p>This is Pluck Polls using REST.</p>

    <PluckIL:Rest runat="server" AppName="Polls" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckPollOnKeyType" Value="article" />
        <PluckIL:WidgetParameter Key="plckPollOnKey" Value="myTestPoll.html" />
    </PluckIL:Rest>

</asp:Content>
