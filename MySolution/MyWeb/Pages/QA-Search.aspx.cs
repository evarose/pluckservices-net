﻿using Pluck.SiteLife.SDK.Models.External;
using Pluck.SiteLife.SDK.Models.Reactions;
using PluckIL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.Pages
{
    public partial class QA_Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string searchTerm = Request["q"].Replace(",", "");

            if (!String.IsNullOrEmpty(searchTerm))
            {
                int totalItems = 0;
                string objectType = String.Empty;
                string pluckStatus = String.Empty;
                Array searchResults = SearchHelpers.SearchPluck("Comment", "Section:Sandbox-Ask-The-Expert AND " + searchTerm, out totalItems, out objectType, out pluckStatus);
                
                if (String.IsNullOrEmpty(pluckStatus))
                {
                    var comments = from c in searchResults.Cast<Comment>().Take(5).ToList()
                                   select new ViewModels.CommentModel
                                   {
                                       ArticleKey = ((ExternalResourceKey)c.CommentedOnKey).Key,
                                       CommentBody = c.Body,
                                       AuthorName = c.Owner.DisplayName,
                                       AuthorAvatarUrl = c.Owner.AvatarPhotoUrl
                                   };
                    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                    string json = jsSerializer.Serialize(comments
                        .Select(c => new { c.ArticleKey, c.CommentBody, c.AuthorAvatarUrl })
                        .ToList());
                    Response.Clear();
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write(json);
                    Response.End();
                }
            }
        }
    }
}