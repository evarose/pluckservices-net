﻿using PluckIL;
using PluckIL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.Pages
{
    public partial class QAComments_Ask : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlCategory.DataSource = QAHelpers.ListCategories();
                ddlCategory.DataTextField = "Value";
                ddlCategory.DataValueField = "Key";
                ddlCategory.DataBind();
            }
        }

        protected void SubmitQuestion(object sender, EventArgs e)
        {
            string baseArticleKey = txtBaseArticleKey.Text;
            if (!String.IsNullOrEmpty(baseArticleKey))
            {
                ViewModels.ArticleModel article = new ViewModels.ArticleModel();
                article.ArticleKey = baseArticleKey;
                article.ArticleTitle = txtQuestionTitle.Text;
                article.ArticleUrl = "http://" + Request.Url.DnsSafeHost + "/Pages/QAC-Question?ID=";
                article.Categories = ddlCategory.SelectedValue;
                article.Section = txtSection.Text;
                string questionBody = txtQuestionBody.Text;
                if (QAHelpers.CreateQuestionAnswerItem(article, questionBody))
                {
                    Response.Redirect(article.ArticleUrl + baseArticleKey);
                }
                else
                {
                    lblMessage.Text = "Error creating Q&A item";
                    plcMessage.Visible = true;
                }

            }
        }
    }
}