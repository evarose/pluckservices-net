﻿<%@ Page Title="Blogs" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Blogs.aspx.cs" Inherits="MyWeb.Blogs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <h3>Pluck Blogs (Multi-Author) using REST.</h3>

    <PluckIL:Rest runat="server" AppName="Blogs" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckBlogType" Value="MultiAuthor" />
        <PluckIL:WidgetParameter Key="pckcbu" Value="http://sandbox.pluck.com" />
    </PluckIL:Rest>

</asp:Content>
