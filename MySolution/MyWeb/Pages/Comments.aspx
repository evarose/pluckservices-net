﻿<%@ Page Title="Comments" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Comments.aspx.cs" Inherits="MyWeb.Comments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <p>This is Pluck Comments using REST.</p>

    <PluckIL:Rest runat="server" AppName="Comments" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckCommentOnKeyType" Value="article" />
        <PluckIL:WidgetParameter Key="plckCommentOnKey" Value="myTestArticle.html" />
    </PluckIL:Rest>

</asp:Content>
