﻿<%@ Page Title="Groups" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Groups.aspx.cs" Inherits="MyWeb.Groups" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <p>This is Pluck Groups using REST.</p>

    <PluckIL:Rest runat="server" AppName="Groups" ShowRestUrl="true" />

</asp:Content>
