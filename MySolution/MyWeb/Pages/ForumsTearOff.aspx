﻿<%@ Page Title="Tear Off Forum" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="ForumsTearOff.aspx.cs" Inherits="MyWeb.Pages.ForumsTearOff" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <p>This is Pluck Forums using REST. Note when calling for a tear-off forum, you will need to pass the following parameters:</p>
    <ul>
        <li>plckForumPage=Forum - this means the root of the forum widget is "Forum". It can also be set to "Category" if you wanted to show all forums in one specific category</li>
        <li>plckForumId=[FORUM_ID] - when plckForumPage=Forum, you would supply plckForumId to only point to a specific forum. If plckForumPage=ForumCategory then you would supply plckCategoryId=[CATEGORY_ID] instead, so that forums widget only shows all forums in one specific category</li>
        <li>pckfpp - this is PCK override where you would specify full URL to the root of your desired forum</li>
    </ul>

    <script type="text/javascript">
        pluckAppProxy.registerActivityCallback("ForumsLoad", hideBreadcrumb);
        pluckAppProxy.registerActivityCallback("ForumsDiscussionListRendered", removeDiscussionLayOver);
        pluckAppProxy.registerActivityCallback("ForumsPostListRendered", removeOtherForums);
        pluckAppProxy.registerActivityCallback("ForumsSimpleSearchRendered", hideBreadcrumb);
        pluckAppProxy.registerActivityCallback("ForumsAdvancedSearchRendered", removeOtherForums);

        function hideBreadcrumb() {
            $('.pluck-forums-title').remove();
            // hide breadcrumb
            $('.pluck-forums-cat-title').children().each(function (index) {
                if (index < 4) {
                    $(this).remove();
                };
            });
        };

        function removeDiscussionLayOver() {
            hideBreadcrumb();
            $('.pluck-forums-discussionList').find('.pluck-forums-main-discussion-icon-col').each(function () {
                $(this).attr('discussionkey', '');
            });
            $('.pluck-forums-discussionList').find('.pluck-forums-main-discussion-name-col .pluck-forums-discussion-title-link').each(function () {
                $(this).attr('discussionkey', '');
            });
        };

        function removeOtherForums() {
            hideBreadcrumb();
            $('.pluck-forums-search-category').children().each(function () {
                if ($(this).prop('selected') != true) {
                    $(this).remove();
                };
            });
            $('.pluck-forums-search-forum').children().each(function () {
                if ($(this).prop('selected') != true) {
                    $(this).remove();
                };
            });
        };
    </script>

    <PluckIL:Rest runat="server" AppName="Forums" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckForumPage" Value="Forum" />
        <PluckIL:WidgetParameter Key="plckForumId" Value="Cat:624A7DAD-4F2A-4cd0-BB6E-DDFC51422846Forum:DEF69A7B-652A-4c0f-86E8-7158F0361917" />
        <PluckIL:WidgetParameter Key="pckfpp" Value="http://sandbox.pluck.com/Pages/ForumsTearOff/" />
    </PluckIL:Rest>

</asp:Content>
