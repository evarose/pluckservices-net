﻿<%@ Page Title="Galleries" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Galleries.aspx.cs" Inherits="MyWeb.Galleries" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <h3>Pluck Galleries (Photos) using REST.</h3>

    <PluckIL:Rest runat="server" AppName="Photos" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckPhotoPage" Value="Galleries" />
    </PluckIL:Rest>

    <h3>Pluck Galleries (Videos) using REST.</h3>

    <PluckIL:Rest runat="server" AppName="Videos" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckPhotoPage" Value="Galleries" />
    </PluckIL:Rest>

</asp:Content>
