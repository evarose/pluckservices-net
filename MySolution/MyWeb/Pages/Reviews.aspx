﻿<%@ Page Title="Reviews" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Reviews.aspx.cs" Inherits="MyWeb.Reviews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %>.</h2>

    <h3>Pluck Reviews (Rollup) using REST.</h3>

    <PluckIL:Rest runat="server" AppName="ReviewsRollup" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckReviewOnKeyType" Value="article" />
        <PluckIL:WidgetParameter Key="plckReviewOnKey" Value="myReviewTest.html" />
    </PluckIL:Rest>

    <h3>Pluck Reviews (List) using REST.</h3>

    <PluckIL:Rest runat="server" AppName="ReviewsList" ShowRestUrl="true">
        <PluckIL:WidgetParameter Key="plckReviewOnKeyType" Value="article" />
        <PluckIL:WidgetParameter Key="plckReviewOnKey" Value="myReviewTest.html" />
    </PluckIL:Rest>

</asp:Content>
