﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Masters/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MyWeb._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Pluck Implementation</h1>
        <p class="lead">Sample site using PluckIL C# Helpers class, with REST & SDK (server-side) implementations. Cool and wonderful!</p>

        <asp:PlaceHolder ID="plcLogin" runat="server">
            <p><a href="/Pages/Login" class="btn btn-primary btn-large">Login &raquo;</a></p>
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="plcLogged" runat="server">
            <p>You are logged in as <asp:Label ID="lblUser" runat="server" Font-Bold="true"></asp:Label></p>

            <p>Check <asp:HyperLink ID="lnkPluckHost" runat="server" Target="_blank"></asp:HyperLink> and see whether you are automatically logged in via Otterfest.</p>

        </asp:PlaceHolder>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Persona</h2>
            <p>
                Pluck Persona is a user profile page. It is called with REST implementation.
            </p>
            <p>
                <a class="btn btn-default" href="/Pages/Persona">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Reviews</h2>
            <p>
                Pluck Reviews page shows 2 REST implementations - one for Rollup and another for List. You can use the same PluckIL for other REST Review types (such as MostHelpful, TopRated etc)
            </p>
            <p>
                <a class="btn btn-default" href="/Pages/Reviews">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Utilities</h2>
            <p>
                Examples of features using SDK implementation (such as Search, Update Articles, etc.)
            </p>
            <p>
                <a class="btn btn-default" href="/Utilities">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
