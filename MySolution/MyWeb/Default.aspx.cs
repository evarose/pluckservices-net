﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PluckIL;
using System.Web.Configuration;

namespace MyWeb
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckLoggedUser();
        }

        protected void CheckLoggedUser()
        {
            plcLogin.Visible = true;
            plcLogged.Visible = false;
            string userKey = SSO.GetATCookieValue("u");
            if (!String.IsNullOrEmpty(userKey))
            {
                lblUser.Text = SSO.GetATCookieValue("a");
                lnkPluckHost.NavigateUrl = WebConfigurationManager.AppSettings["PluckHost"];
                lnkPluckHost.Text = WebConfigurationManager.AppSettings["PluckHost"];
                plcLogged.Visible = true;
                plcLogin.Visible = false;
            }
        }
    }
}