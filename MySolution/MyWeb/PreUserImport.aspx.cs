﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb
{
    public class FBUser
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
    }

    public class PluckUser
    {
        public string UserKey { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public string Sig { get; set; }
    }

    public partial class PreUserImport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PluckUser pluckUser = new PluckUser();

            if (Request.Form.Count > 0)
            {
                if (Request.Form["fbUser"] != null)
                {
                    FBUser fbUser = JsonConvert.DeserializeObject<FBUser>(Request.Form["fbUser"]);
                    pluckUser.UserKey = "fb_" + fbUser.ID + "_fb"; // fb_xxxx_fb is Pluck User Key format that comes from FB
                    pluckUser.DisplayName = fbUser.FirstName + " " + fbUser.LastName;
                    pluckUser.Email = fbUser.Email;
                    pluckUser.AvatarUrl = "https://graph.facebook.com/" + fbUser.ID + "/picture?type=large";
                    pluckUser.Sig = CreateSignature(pluckUser);
                }

                // POST data from Pluck, handle it here
                List<string> postData = new List<string>();
                foreach (string key in Request.Form.AllKeys)
                {
                    string postLine = key + ":" + Request.Form[key];
                    postData.Add(postLine);
                    lblPostData.Text += postLine + "<br />";
                }
                string postFilePath = Server.MapPath("_PostData.txt");
                lblPostFile.Text = postFilePath;
                System.IO.File.WriteAllLines(postFilePath, postData);
                panPostData.Visible = true;
            }

            if (Request.QueryString.Count > 0)
            {
                // GET data via Query String (for test purpose only) 
                // Example: ?fbUser={"id":"100004559707544","email":"eva.rahim@demandmedia.com","first_name":"Eva","gender":"female","last_name":"Fba","link":"https://www.facebook.com/eva.pluck.7","locale":"en_US","name":"Eva Fba","timezone":0,"updated_time":"2013-01-18T11:49:58+0000","username":"eva.pluck.7","verified":true}

                if (Request.QueryString["fbUser"] != null)
                {
                    FBUser fbUser = JsonConvert.DeserializeObject<FBUser>(Request.QueryString["fbUser"]);
                    pluckUser.UserKey = "fb_" + fbUser.ID + "_fb"; // fb_xxxx_fb is Pluck User Key format that comes from FB
                    pluckUser.DisplayName = fbUser.FirstName + " " + fbUser.LastName;
                    pluckUser.Email = fbUser.Email;
                    pluckUser.AvatarUrl = "https://graph.facebook.com/" + fbUser.ID + "/picture?type=large";
                    pluckUser.Sig = CreateSignature(pluckUser);
                }

                List<string> getData = new List<string>();
                foreach (string key in Request.QueryString.AllKeys)
                {
                    string getLine = key + ":" + Request.QueryString[key];
                    getData.Add(getLine);
                    lblGetData.Text += getLine + "<br />";
                }

                string getFilePath = Server.MapPath("_GetData.txt");
                lblGetFile.Text = getFilePath;
                System.IO.File.WriteAllLines(getFilePath, getData);
                panGetData.Visible = true;
            }

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write("{\"SignedUserToken\":" + JsonConvert.SerializeObject(pluckUser) + "}");
            Response.End();
        }

        private static string CreateSignature(PluckUser pluckUser)
        {
            string strToHash = pluckUser.UserKey + pluckUser.DisplayName + pluckUser.Email + pluckUser.AvatarUrl + ConfigurationManager.AppSettings["PluckSharedSecret"];
            return CalculateMD5Hash(strToHash);
        }

        private static string CalculateMD5Hash(string input)
        {
            // Step 1, calculate MD5 hash from input
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(input);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);

            // Step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encodedBytes.Length; i++)
            {
                sb.Append(encodedBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }

    
}