﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using PluckIL;

namespace MyWeb
{
    public partial class SiteMaster : MasterPage
    {
        protected string pluckHost = WebConfigurationManager.AppSettings["PluckHost"];
        public string pluckLibraryQueryString = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            string message;
            dynamic color;
            if (PluckConfig.CheckPluckStatus(out color, out message))
            {
                CheckLogging();
                CheckLoggedUser();
            }
        }

        protected void CheckPluckConfig()
        {
            if (!PluckConfig.IsPluckConfigured())
            {
                plcMessage.Visible = true;
                lblMessage.Text = CustomMessage.PluckNotConfigured;
            }
        }

        protected void CheckLogging()
        {
            if (PluckConfig.IsLoggingConfigured())
            {
                if (!PluckConfig.IsLoggingEnabled())
                {
                    plcMessage.Visible = true;
                    lblMessage.Text = CustomMessage.LoggingDisabled;
                }
            }
            else
            {
                plcMessage.Visible = true;
                lblMessage.Text = CustomMessage.LoggingNotConfigured;
            }
        }

        protected void CheckLoggedUser()
        {
            plcLogin.Visible = true;
            plcLogout.Visible = false;
            string userKey = SSO.GetATCookieValue("u");
            if (!String.IsNullOrEmpty(userKey))
            {
                if (PluckConfig.ATCookieIsHttpOnly)
                    pluckLibraryQueryString = "?ath=" + SSO.GetATCookieValue("h");
                lblUser.Text = SSO.GetATCookieValue("a");
                plcLogin.Visible = false;
                plcLogout.Visible = true;
            }
        }
    }
}