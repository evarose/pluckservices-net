from Pluck.SiteLife.Daapi.Requests.Blogs import BlogsPageRequest
from Pluck.SiteLife.Models.Blog import BlogType
from Pluck.SiteLife.Models.Prefs import SiteLifePrefs
import json as simplejson
import hashlib

def post(request, response):
	__log = get_logger("fbCallbackCatcher.py")

	fbUserString = request.get("fbUser", "{}")
	pluckUsersString = request.get("pluckUsers", "")
	access_token = request.get("access_token")
	sig = request.get("sig")

	__log.info("Caught Facebook pre user import callback.   fbUser: %s \n pluckUsers: %s \n access_token: %s \n sig: %s" % (fbUserString, pluckUsersString, access_token, sig))
	validateSig(fbUserString, pluckUsersString, access_token, sig)

	if(pluckUsersString == ""):
		pluckUsersString = "[]"

	fbUser = simplejson.loads(fbUserString)
	pluckUsers = simplejson.loads(pluckUsersString)

	rules_file = open(WidgetFactory.RootDirectory.FullName + "\\WidgetBundles\\otterfest\\fbPreImportCallbackCatcher\\fb_user_import_rules.txt", "r")
	rules_dict = simplejson.loads(rules_file.read())

	resp = {"ClientSideLogin": {"reason": "Facebook user ID %s was not found in fb_user_import_rules.txt" % fbUser["id"]}}
	if rules_dict.has_key(fbUser["id"]):
		resp = rules_dict[fbUser["id"]]
		if (resp.has_key("Cookies")):
			for cookie in resp["Cookies"]:
				response.set_cookie(name=cookie["Name"], data=cookie["Value"], days=cookie["Age"])

		if ((resp.has_key("SignedUserToken")) and (not resp["SignedUserToken"].has_key("Sig"))):
			resp["SignedUserToken"]["Sig"] = sign(resp["SignedUserToken"])

	__log.info("pre-import callback responding with: %s" % simplejson.dumps(resp))
	
	render_json(resp, response)

def sign(token):
	hashMe = "%s%s%s%s%s" % (token["UserKey"], token["DisplayName"], token["Email"], token["AvatarUrl"], SiteLifePrefs.Instance.UrlPrefs.SharedSecret)
	return hashlib.md5(hashMe).hexdigest()

def validateSig(fbUser, pluckUsers, access_token, sig):
	__log = get_logger("fbCallbackCatcher.py")

	hashMe = "%s%s%s%s" % (fbUser, pluckUsers, access_token, SiteLifePrefs.Instance.UrlPrefs.SharedSecret)
	hash = hashlib.md5(hashMe).hexdigest()
	
	if hash != sig:
		__log.error("bad hash.  got: %s, epxected: %s" % (sig, hash))
	else:
		__log.info("hash is good")
