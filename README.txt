Using ASP.NET Web Application with PluckIL
------------------------------------------

Building your Visual Studio application
---------------------------------------
1. From your Visual Studio environment, open up "MySolution.sln" file
2. There are 2 projects in the solution:
   - MyWeb - contains the sample ASP.NET Web Application
   - PluckIL - contains PluckIL C# Helpers
3. First, build PluckIL. You will get an error about "Pluck namespace cannot be found". This is due to Pluck NET SDK is missing.
4. Get the correct .NET SDK file for your environment. If say you are referencing to Pluck libraries at http://presales.pluck.com then download the SDK from http://presales.pluck.com/ver1.0/views/common/csharp_sdk.zip. The ZIP file contains DLL files that you need to successfully build PluckIL library. Add missing DLL's into PluckIL References folder and build again. The build should go through fine.
5. Once PluckIL is built, then build MyWeb project. First, please ensure that PluckIL.dll is in MyWeb -> References. If not, you will need to add Project Reference to PluckIL project. To do this, right click "References" -> "Add Reference" -> "Solution" and select PluckIL project from there.
6. Once all built, you need to setup local IIS website so that you can view this website locally

Setting up local IIS website
----------------------------
1. Before setting up, check Web.Config file in MyWeb project, and you will notice that PluckHost is set to http://presales.pluck.com - this is where Pluck platform that your website is communicating to. So with that in mind, you will need to create a local IIS site within the same domain, so that Pluck SSO works. If your PluckHost is a different URL, then change them here - see "Setting up Web.Config variables" section below.
2. So, in your local host file (c:\Windows\System32\drivers\etc\hosts), enter the following:

127.0.0.1	sandbox.pluck.com

3. Then, go ahead to your IIS snap-in console, and add a new website. Points this new website to "MyWeb" folder.
4. By default, IIS will create the website to run under NET Framework version 2. You need to change this to NET Framework version 4. To do this, go to "Application Pools" in IIS and locate where your website is, and then double click to change the NET Framework version in "Edit Application Pool" dialog.
5. Once all ready to go, open up your browser, and enter http://sandbox.pluck.com and you will be able to see "MyWeb" website.

Setting up Web.Config variables
-------------------------------
1. Before you start using your website, you will need to enter important data to communicate with Pluck. So, open up your Web.Config file in your "MyWeb" application and copy & paste the following under <configuration>:

<appSettings>
	<!--
		PluckEnabled = Turn on (true) or off (false)
		PluckLoggingEnabled = Turn on (true) or off (false)
		PluckHost = Where your Pluck platform lives
		PluckDomain = The top-level-domain (TLD) of your Pluck server. Ensure to put a dot at the beginning
		PuckSharedSecret = The secret key unique to your environment
		PluckEditorKey = The user key of an Editor in Pluck
		PluckEditorName = The user display name of an Editor in Pluck
		PluckEditorEmail = The user email of an Editor in Pluck
	-->
	<add key="PluckEnabled" value="true"/>
	<add key="PluckLoggingEnabled" value="true" />
	<add key="PluckHost" value="http://presales.pluck.com"/>
	<add key="PluckDomain" value=".pluck.com"/>
	<add key="PluckSharedSecret" value="cd280581-64f9-4951-98c0-d33f0def1234"/>
	<add key="PluckEditorKey" value="evaeditor"/>
	<add key="PluckEditorName" value="Eva Pluck"/>
	<add key="PluckEditorEmail" value="eva.rahim@pluck.com"/>
</appSettings>

2. Make necessary changes above according to your Pluck environment.

NOW, YOU ARE READY TO USE PluckIL HELPER CLASS FOR YOUR .NET IMPLEMENTATION